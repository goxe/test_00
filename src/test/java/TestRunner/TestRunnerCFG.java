package TestRunner;
import TestRunner.library.BrowserLibrary.TestBase;
import TestRunner.library.Utility;
import org.testng.annotations.*;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;



@CucumberOptions(features= "Features",glue={"StepDefinition"},tags ={"@google"},
        format={"pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/CucumberTestReport.json",
                "rerun:target/cucumber-reports/re-run.txt"}
)



public class TestRunnerCFG extends TestBase
{
     TestNGCucumberRunner testRunner;

     public String obj2;

     public TestRunnerCFG()
     {

         String directoryPath=System.getProperty("user.dir")+"src\\test\\resources\\Logs\\EMoney Automation Logs";

         obj2 = directoryPath;

         Utility.createDirectory(directoryPath);

     }

    @BeforeClass
    public void setUP()
    {
        testRunner= new TestNGCucumberRunner(TestRunnerCFG.class);

    }
    @Test(description="login",dataProvider="features")
    public void login(CucumberFeatureWrapper cFeature)
    {
        testRunner.runCucumber(cFeature.getCucumberFeature());
    }

    @DataProvider(name="features")
    public Object[][] getFeatures()
    {
        return testRunner.provideFeatures();
    }


    @AfterClass
    public void tearDown()
    {
        testRunner.finish();
    }

    @AfterTest
    public void runBatch (){

    }
}
