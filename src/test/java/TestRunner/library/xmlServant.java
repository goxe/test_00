package TestRunner.library;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.File;

public class xmlServant {


    public static String xmlServanttest (String keyTag, String keyValue, String valueTag, String filePath) {

        String result1 = "";

        try {
            File inputFile = new File(System.getProperty("user.dir") + filePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;

            dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            XPath xPath = XPathFactory.newInstance().newXPath();
            parser resolver = new parser();
            resolver.addVariable(new QName(null, keyTag), keyValue);
            xPath.setXPathVariableResolver(resolver);

            XPathExpression expr = xPath.compile("/ArrayOfKeyValue/KeyValue[Key=$" + keyTag + "]");
            Object result = expr.evaluate(doc, XPathConstants.NODESET);

            NodeList nodes = (NodeList) result;
            for (int i = 0; i < nodes.getLength(); i++) {
                Node nNode = nodes.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    result1 = eElement.getElementsByTagName(valueTag).item(0).getTextContent();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result1;
    }
}



