package TestRunner.library;

import org.openqa.selenium.winium.WiniumDriver;

import java.awt.*;
import java.awt.event.KeyEvent;

public class servant {

    public static void maximizeWindow(WiniumDriver driver){
      try {
        Robot r = new Robot();
        r.keyPress(KeyEvent.VK_WINDOWS);
        r.keyPress(KeyEvent.VK_UP);
        r.keyRelease(KeyEvent.VK_UP);
        r.keyRelease(KeyEvent.VK_WINDOWS);
    } catch (Exception e) {
        e.printStackTrace();
    }

    }
    public static void quitWindow(WiniumDriver driver){
        try {
            Robot r = new Robot();
            r.keyPress(KeyEvent.VK_ALT);
            r.keyPress(KeyEvent.VK_F4);
            r.keyRelease(KeyEvent.VK_F4);
            r.keyRelease(KeyEvent.VK_ALT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
