package TestRunner.library;

import TestRunner.TestRunnerCFG;
import cucumber.api.Scenario;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.winium.WiniumDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {

    public static void createDirectory(String directoryPath)        {


        //String logFolder ="M:\\IT\\Staff\\Automation\\AutomationLogs\\"+time;

        File theDir = new File(directoryPath);
        if (!theDir.exists()) {

            try{
                theDir.mkdirs();
            }
            catch(SecurityException se){
                //basarisiz
            }
        }
    }


    public static void logger(String input, String logName, String status){

        TestRunner.TestRunnerCFG str = new TestRunner.TestRunnerCFG();

        BufferedWriter writer = null;
        try
        {
            Date now = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd - HH mm ss");
            String time = dateFormat.format(now);

            writer = new BufferedWriter(new FileWriter(str.obj2+"\\"+time+"_"+logName+"_"+status+".txt"));
            writer.write(input);

        }
        catch (IOException e)
        {
            System.out.println("Hata:"+e.getMessage());
        }
        finally
        {
            try
            {
                if ( writer != null)
                    writer.close( );
            }
            catch ( IOException e)
            {
                System.out.println("Hata2");
            }
        }
    }

    public static void xmlLogger(String input, String filePath){


        BufferedWriter writer = null;
        try
        {

            writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(input);

        }
        catch (IOException e)
        {
            System.out.println("Hata:"+e.getMessage());
        }
        finally
        {
            try
            {
                if ( writer != null)
                    writer.close( );
            }
            catch ( IOException e)
            {
                System.out.println("Hata2");
            }
        }
    }




    public static void captureSecreenshot(WebDriver driver, String scenarioName){

        try {

            Date now = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd - HH mm ss");
            String time = dateFormat.format(now);
            String logName;
//            File dir = new File(time);
//            dir.mkdir();


            Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
            ImageIO.write(fpScreenshot.getImage(), "PNG", new File(System.getProperty("user.dir") +"\\src\\test\\resources\\Snapshots\\"+scenarioName+"  "+time+".png"));


            System.out.println("Hata sonucu ekran goruntusu basariyla alindi! Dosya adi: "+time);
        }
        catch (Exception e){
            System.out.println("Ekran goruntusu alinirken hata olustu!"+e.getMessage());
        }
        }



//    public static void captureSecreenshot(WebDriver driver){
//
//        try {
//
//            TakesScreenshot ts=(TakesScreenshot)driver;
//
//            File source = ts.getScreenshotAs(OutputType.FILE);
//
//            FileUtils.copyFile(source, new File("C:\\Users\\goktug.erdogan\\IdeaProjects\\WDTestAutomation\\src\\test\\resources\\Snapshots\\FailureSnapshot.png"));
//
//            System.out.println("Hata sonucu ekran goruntusu basarıyla alındı!");
//
//
//        } catch (Exception e) {
//            System.out.println("Ekran goruntusu alinirken hata olustu!"+e.getMessage());
//
//        }
//    }



    public static void appScreenShot(WiniumDriver driver) throws Exception {
        Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage capture = new Robot().createScreenCapture(screenRect);
        ImageIO.write(capture, "png", new File(System.getProperty("user.dir") +"\\src\\test\\resources\\Snapshots\\FailureSnapshot.png"));
    }

}
