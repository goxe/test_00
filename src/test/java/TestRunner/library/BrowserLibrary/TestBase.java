package TestRunner.library.BrowserLibrary;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;


public class TestBase {

    protected WebDriverWait wait;

    //Test setup'� set edilir
    @BeforeMethod
    @Parameters(value={"browser"})
    public void setupTest (@Optional String browser) throws MalformedURLException {
        //Browser set edilir
        TLDriverFactory.setTLDriver(browser);
        wait = new WebDriverWait(TLDriverFactory.getTLDriver(), 15);
    }

    @AfterMethod
    public synchronized void tearDown() throws Exception {
        TLDriverFactory.getTLDriver().quit();
    }

}
