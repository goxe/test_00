package TestRunner.library.BrowserLibrary;

import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverInfo;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class OptionsManager {

    //Chrome Options
    //TODO kodu temizle
    public static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-popup-blocking");
        //options.addArguments("--incognito");
        return options;
        /*ChromeDriverService service = new ChromeDriverService.Builder()
                .usingAnyFreePort()
                .build();
        ChromeDriver driver = new ChromeDriver(service, options);*/
    }

    //Firefox Options
    public static FirefoxOptions getFirefoxOptions () {
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile();
        //Sertifika duzenlemeleri
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(false);
        //Proxy ayari kullanmama
        profile.setPreference("network.proxy.type", 0);
        //Firefox profili set edilir
        options.setCapability(FirefoxDriver.PROFILE, profile);
        return options;
    }


    //TODO ie opstion duzenle
//    public static InternetExplorerOptions getInternetExplorerOptions () {
//        InternetExplorerOptions options = new InternetExplorerOptions();
//
//        DesiredCapabilities capabilities =  DesiredCapabilities.internetExplorer();
//        capabilities.setBrowserName("internet explorer");
////        capabilities.setPlatform(Platform.WINDOWS);
////        capabilities.setVersion("11");
//        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//        capabilities.setCapability("acceptInsecureCerts", true);
//        capabilities.setCapability("acceptSslCerts",true);
//        capabilities.setAcceptInsecureCerts(true);
//        capabilities.acceptInsecureCerts();
//
//        Accept Untrusted Certificates
//        profile.setAcceptUntrustedCertificates(true);
//        profile.setAssumeUntrustedCertificateIssuer(false);
//        Use No Proxy Settings
//        profile.setPreference("network.proxy.type", 0);
//        Set Firefox profile to capabilities
//        options.setCapability(InternetExplorerDriver.PROFILE, profile);
//        return options;
//    }
}
