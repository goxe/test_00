package TestRunner.library.BrowserLibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;


public class TLDriverFactory {

    private static TestRunner.library.BrowserLibrary.OptionsManager OptionsManager = new OptionsManager();
    private static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

    public static synchronized void setTLDriver(String browser) {
        if (browser.equals("firefox")) {
            //Tekil thread kullanimi icin
//            tlDriver = ThreadLocal.withInitial(() -> new FirefoxDriver(com.OptionsManager.getFirefoxOptions()));

            //Dcokerize remote driver kullanimi
            try {
                tlDriver.set(new RemoteWebDriver(new URL("http://3.15.198.188:4444/wd/hub"), TestRunner.library.BrowserLibrary.OptionsManager.getFirefoxOptions()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else if (browser.equals("chrome")) {
            //Tekil thread kullanimi icin
//            tlDriver.set(new ChromeDriver(com.OptionsManager.getChromeOptions()));

            //Dcokerize remote driver kullanimi
            try {
                tlDriver.set(new RemoteWebDriver(new URL("http://3.15.198.188:4444/wd/hub"), TestRunner.library.BrowserLibrary.OptionsManager.getChromeOptions()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

            //Dockerize kullanim ie
//            try {
//                tlDriver.set(new RemoteWebDriver(new URL("http://10.4.2.20:4444/wd/hub"), TestRunner.library.BrowserLibrary.OptionsManager.getInternetExplorerOptions()));
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }

    }

    //Webdriver bekleme suresi
    //Todo sureyi optimize et (15 saniye)
    public static synchronized WebDriverWait getWait(WebDriver driver) {
        return new WebDriverWait(driver, 30);
    }

    public static synchronized WebDriver getTLDriver() {
        return tlDriver.get();
    }
}