//package TestRunner;
//
//import cucumber.api.CucumberOptions;
//import cucumber.api.testng.CucumberFeatureWrapper;
//import cucumber.api.testng.TestNGCucumberRunner;
//import org.openqa.selenium.WebDriver;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//@CucumberOptions(features= "Features",glue={"StepDefinition"}, tags = {"@test"},
//        format={"pretty",
//                "html:target/cucumber-reports/cucumber-pretty",
//                "json:target/cucumber-reports/CucumberTestReport.json",
//                "rerun:target/cucumber-reports/re-run.txt"}
//)
//
//public class TestRunnerCFG2
//{
//     WebDriver driver;
//     TestNGCucumberRunner testRunner;
//
//    @BeforeClass
//    public void setUP()
//    {
//        testRunner= new TestNGCucumberRunner(TestRunnerCFG2.class);
//
//    }
//    @Test(description="login",dataProvider="features")
//    public void login(CucumberFeatureWrapper cFeature)
//    {
//        testRunner.runCucumber(cFeature.getCucumberFeature());
//    }
//    @DataProvider(name="features")
//    public Object[][] getFeatures()
//    {
//        return testRunner.provideFeatures();
//    }
//
//
//    @AfterClass
//    public void tearDown()
//    {
//        testRunner.finish();
//    }
//
//
//}
