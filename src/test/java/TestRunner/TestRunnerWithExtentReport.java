//package TestRunner;
//import TestRunner.library.BrowserLibrary.TestBase;
//import TestRunner.library.Utility;
//import com.vimalselvam.cucumber.listener.Reporter;
//import cucumber.api.junit.Cucumber;
//import org.junit.runner.RunWith;
//import org.testng.annotations.*;
//
//import cucumber.api.CucumberOptions;
//import cucumber.api.testng.CucumberFeatureWrapper;
//import cucumber.api.testng.TestNGCucumberRunner;
//import com.vimalselvam.cucumber.listener.Reporter;
//
//@CucumberOptions(features={"Features"}, glue={"StepDefinition"},
//        format={"pretty",
//                "html:target/cucumber-reports/cucumber-pretty",
//                "json:target/cucumber-reports/CucumberTestReport.json",
//                "rerun:target/cucumber-reports/re-run.txt"},
//        plugin = {"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:"}
////        plugin = {"com.vimalselvam.cucumber.ExtentCucumberFormatter:output/report.html"}
//)
//
//
//public class TestRunnerWithExtentReport extends TestBase
//{
//    TestNGCucumberRunner testRunner;
//
//
//    @BeforeClass
//    public void setUP()
//    {
//        testRunner= new TestNGCucumberRunner(TestRunnerWithExtentReport.class);
//
//    }
//    @Test(description="login",dataProvider="features")
//    public void login(CucumberFeatureWrapper cFeature)
//    {
//        testRunner.runCucumber(cFeature.getCucumberFeature());
//    }
//
//    @DataProvider(name="features")
//    public Object[][] getFeatures()
//    {
//        return testRunner.provideFeatures();
//    }
//
//
//    @AfterClass
//        public static void writeExtentReport() {
//        Reporter.loadXMLConfig("src/test/java/TestRunner/extent-config.xml");
//        Reporter.setSystemInfo("user", System.getProperty("user.name"));
//        Reporter.setSystemInfo("Reports", "Created by Wirecard Automation Robot");
//        Reporter.setTestRunnerOutput("Test Automation");
//    }
//
//
//    @AfterTest
//    public void runBatch (){
//
//    }
//}