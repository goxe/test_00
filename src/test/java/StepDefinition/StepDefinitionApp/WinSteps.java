package StepDefinition.StepDefinitionApp;

import TestRunner.library.Utility;
import TestRunner.library.servant;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.openqa.selenium.winium.WiniumDriverService;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class WinSteps {

    WiniumDriver driver;

    @Given("^Run \"([^\"]*)\" application$")
    public void Run_Application(String word) throws Throwable {

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\deneme.properties");
        obj.load(objfile);

        File driverPath = new File("C:\\Software\\Winium Desktop Driver\\Winium.Desktop.Driver.exe");
        WiniumDriverService service = new WiniumDriverService.Builder().usingDriverExecutable(driverPath).usingPort(9999).withVerbose(true).withSilent(false).buildDesktopService();
        service.start();

        DesktopOptions options = new DesktopOptions();
        options.setApplicationPath(obj.getProperty(word));

        driver = new WiniumDriver(service, options);
        Thread.sleep(2000);
        servant.maximizeWindow(driver);
    }


    @And("^Click ([^\"]*) in app$")
    public void click_setID_button_In_App(String word) throws Throwable {

//        for (int i = 1; i <= 3; i++) {
//            try {
                driver.findElement(By.name(word)).click();
//                return;
//            } catch (Exception e) {
//                if (i <= 2) {
//                    Thread.sleep(2000);
//                } else {
//                    Utility.appScreenShot(driver);
//                    throw new Throwable();
//                }
//            }
//        }
    }

//        try {
//            driver.findElement(By.name(word)).click();
//        } catch (Exception e) {
//            Thread.sleep(2000);
//            try {
//                driver.findElement(By.name(word)).click();
//            } catch (Exception x) {
//                Thread.sleep(2000);
//                try {
//                    driver.findElement(By.name(word)).click();
//                } catch (Exception c) {
//
//                    Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
//                    BufferedImage capture = new Robot().createScreenCapture(screenRect);
//                    ImageIO.write(capture, "png", new File("C:\\Users\\goktug.erdogan\\IdeaProjects\\WDTestAutomation\\src\\test\\resources\\Snapshots\\FailureSnapshot.png"));
//
//                    throw new Throwable();
//                }
//            }
//        }


//    @And("^Wait for ([^\"]*) seconds$")
//    public void wait(int word) throws Throwable {
//
//        TimeUnit.SECONDS.sleep(word);
//    }

    @And("^And Close app$")
    public void Close_App() throws Throwable {

        servant.quitWindow(driver);

    }
}
