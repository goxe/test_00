package StepDefinition;

import TestRunner.library.BrowserLibrary.TLDriverFactory;
import TestRunner.library.xmlServant;
import cucumber.api.java.en.And;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class MM_Steps {


    @And("^Get value and set to mmtspApproveXml with \"([^\"]*)\" file$")
    public void get_Value_and_set_mmtspApprovalXml(String filePath) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        String xid = xmlServant.xmlServanttest("Key", "xid", "Value",obj.getProperty(filePath));

        String cavv = xmlServant.xmlServanttest("Key", "cavv", "Value",obj.getProperty(filePath));

        String md = xmlServant.xmlServanttest("Key", "md", "Value",obj.getProperty(filePath));


        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty("MM_xmlData"));

        while (rs.next()) {

            String orderIdInput = rs.getString("OrderId");

            String mmtspInput = "<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?><WIRECARD><ServiceType>CCSale</ServiceType><OperationType>ProcessPaymentMMTSP</OperationType><Token><UserCode>20104</UserCode><Pin>189A3B7DF0DC49718B2612zly</Pin></Token><OrderId>orderIdInput</OrderId><MPAY>test</MPAY><CreditCardNo>md</CreditCardNo><PayerSecurityLevel>05</PayerSecurityLevel><PayerTxnId>xid</PayerTxnId><PayerAuthenticationCode>cavv</PayerAuthenticationCode></WIRECARD>";

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty("inputField"))).sendKeys(mmtspInput.replace("orderIdInput", orderIdInput).replace("xid", xid).replace("cavv", cavv).replace("md", md));
        }
    }

    @And("^Get value and set to tspApproveXml$")
    public void get_Value_and_set_tspApprovalXml() throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty("tspStatus"));

        while (rs.next()) {

            String tspOrderId = rs.getString("ObjectId");

            String tspInput = obj.getProperty("tspApproveXml");

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty("inputField"))).sendKeys(tspInput.replace("tspOrderId", tspOrderId));
        }
    }

    @And("^Get value and set to mmSubPartner3DSApprovment with \"([^\"]*)\" file$")
    public void get_Value_and_set_mmSubPartner3DSApprovment(String filePath) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        String xid = xmlServant.xmlServanttest("Key", "xid", "Value",obj.getProperty(filePath));

        String cavv = xmlServant.xmlServanttest("Key", "cavv", "Value",obj.getProperty(filePath));

        String md = xmlServant.xmlServanttest("Key", "md", "Value",obj.getProperty(filePath));


        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty("MMSP_xmlData"));

        while (rs.next()) {

            String orderIdInput = rs.getString("OrderId");

            String mmtspInput = "<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?><WIRECARD><ServiceType>CCSale</ServiceType><OperationType>ProcessPaymentMMTSP</OperationType><Token><UserCode>20104</UserCode><Pin>189A3B7DF0DC49718B2612zly</Pin></Token><OrderId>orderIdInput</OrderId><MPAY>test</MPAY><CreditCardNo>md</CreditCardNo><PayerSecurityLevel>05</PayerSecurityLevel><PayerTxnId>xid</PayerTxnId><PayerAuthenticationCode>cavv</PayerAuthenticationCode></WIRECARD>";

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty("inputField"))).sendKeys(mmtspInput.replace("orderIdInput", orderIdInput).replace("xid", xid).replace("cavv", cavv).replace("md", md));

        }
    }

    @And("Execute ccProxySale3DsecObjectId sql and set ReleasePayment xml")
    public void trsq_sql() throws Throwable {
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty("mmSubPartnerPayment_Sql"));

        while (rs.next()) {
            try {
                WebDriverWait wait = new WebDriverWait(TLDriverFactory.getTLDriver(), 50);
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"txtInput\"]")));

                String objectId = rs.getString("ObjectId");

                String input = "<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?><WIRECARD><ServiceType>CCMarketPlace</ServiceType><OperationType>ReleasePayment</OperationType><Token><UserCode>20104</UserCode><Pin>189A3B7DF0DC49718B2612zly</Pin></Token><SubPartnerId>868</SubPartnerId><CommissionRate /><MPAY>otomasyon</MPAY><OrderId>objectId</OrderId><Description>otomasyon</Description></WIRECARD>";

                TLDriverFactory.getTLDriver().findElement(By.xpath("//*[@id=\"txtInput\"]")).sendKeys(input.replace("objectId", objectId));
            } catch (Exception e) {

                throw new Throwable();
            }
        }
    }

    @And("Execute releaseControl sql and check statusId")
    public void check_status_Id_2() throws Throwable {
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty("mmSubPartnerPayment_Sql"));

        while (rs.next()) {
            String ObjectId = rs.getString("ObjectId");

            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            Connection con2 = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
            Statement sqlStatement2 = con2.createStatement();
            ResultSet rs2 = sqlStatement2.executeQuery(obj.getProperty("releaseControl") + "'" + ObjectId + "'");

            while (rs2.next()) {
                String statusId = rs2.getString("StatusId");

                if ("2".equals(statusId)) {

                    System.out.println();
                } else {
                    throw new Throwable();
                }
            }
        }
    }
}


