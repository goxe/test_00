package StepDefinition;

import TestRunner.library.BrowserLibrary.TLDriverFactory;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import io.github.bonigarcia.wdm.WebDriverManager;
import TestRunner.library.Utility;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;


public class Steps {

    private static WebDriver driver;

    static WebDriver getDriver() {

        return driver;
    }


//    @After
//    public void embedScreenshot(Scenario scenario) {
//        if (scenario.isFailed()) {
//            try {
//                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
//                scenario.embed(screenshot, "image/png");
//
//            } catch (WebDriverException wde) {
//                System.out.println();
//            }
//
//        }
//    }

    @After
    public static byte[] takeFailureScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                BufferedImage image = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(TLDriverFactory.getTLDriver()).getImage();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(image, "png", baos);

                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                baos.close();
                scenario.embed(imageInByte, "image/png");
                return imageInByte;
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return "Unable to Get Screenshot.".getBytes();
    }


    @Given("^Open the Firefox and launch the application$")
    public void open_the_Firefox_and_launch_the_application() throws Throwable {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\goktug.erdogan\\IdeaProjects\\WDTestAutomation\\geckodriver.exe");
        driver = new FirefoxDriver();
        TLDriverFactory.getTLDriver().manage().window().maximize();
        TLDriverFactory.getTLDriver().get("https://google.com");

    }


    @When("^Enter the search words$")
    public void enter_the_search_words() throws Throwable {
        TLDriverFactory.getTLDriver().findElement(By.xpath("//*[@id=\"lst-ib\"]")).sendKeys("WIRECARD");
    }


    @And("^Click search button$")
    public void click_search_button() throws Throwable {
        TLDriverFactory.getTLDriver().findElement(By.xpath("/html/body/div/div[3]/form/div[2]/div[3]/center/input[1]")).click();
    }


    @Then("^I See WIRECARD text$")
    public void i_see_wirecard_test() throws Throwable {
        TLDriverFactory.getTLDriver().findElement(By.xpath("/html/body/div[6]/div[3]/div[10]/div[1]/div[2]/div/div[2]/div[2]/div/div/div/div[1]/div/div[1]/div/div/div[1]/a/h3"));
    }


    @When("^Open the Firefox and go to sgate$")
    public void open_the_Firefox_and_go_to_sgate() throws Throwable {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\goktug.erdogan\\IdeaProjects\\WDTestAutomation\\geckodriver.exe");
        driver = new FirefoxDriver();
        TLDriverFactory.getTLDriver().manage().window().maximize();
        TLDriverFactory.getTLDriver().get("https://test.wirecard.com.tr/sgate/test.aspx");
    }


    @And("^Open service list$")
    public void open_service_list() throws Throwable {
        TLDriverFactory.getTLDriver().findElement(By.xpath("//*[@id=\"ddlService\"]")).click();
    }


    @And("^Check input text contains WDSG00005")
    public void check_input_xml_contains_WDSG00005() throws Throwable {
        TLDriverFactory.getTLDriver().findElement(By.xpath("//*[@id=\"txtResult\"][text()[contains(.,'<Result>\n" +
                "  <Item Key=\"StatusCode\" Value=\"1\" />\n" +
                "  <Item Key=\"ResultCode\" Value=\"WDSG00005\" />\n" +
                "  <Item Key=\"ResultMessage\" Value=\"Hata Olu?tu.\" />\n" +
                "</Result>')]]"));
    }


//    <Dynamic Url set-up>

    @When("^Open the Firefox and go to ([^\"]*)$")
    public void open_the_Firefox_and_go_to_Url(String word) throws Throwable {

//        WebDriverManager.firefoxdriver().setup();

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);

        try {
//            driver = new FirefoxDriver();
            TLDriverFactory.getTLDriver().manage().window().maximize();
            TLDriverFactory.getTLDriver().get(obj.getProperty(word));
        } catch (Exception e) {


            throw new Throwable();
        }

    }


    @When("^Open the Chrome and go to ([^\"]*)$")
    public void open_the_Chrome_and_go_to_Url(String word) throws Throwable {

//        WebDriverManager.chromedriver().setup();

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);

        try {
//            driver = new ChromeDriver();
            TLDriverFactory.getTLDriver().manage().window().maximize();
            TLDriverFactory.getTLDriver().get(obj.getProperty(word));
        } catch (Exception e) {


            throw new Throwable();
        }

    }

    @When("^Go ([^\"]*) url$")
    public void go_url(String word) throws Throwable {

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);

        try {
            TLDriverFactory.getTLDriver().get(obj.getProperty(word));
        } catch (Exception e) {

            throw new Throwable();
        }

    }


//    </Dynamic Url set-up>


//    <Dynamic object based text control>

    @And("^\"([^\"]*)\" element contains ([^\"]*) text$")
    public void XPath_contains_text(String word, String word2) throws Throwable {

        TLDriverFactory.getTLDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(obj.getProperty(word)), obj.getProperty(word2)));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word) + "[text()[contains(.," + "'" + obj.getProperty(word2) + "'" + ")]]"));
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^\"([^\"]*)\" element contains \"([^\"]*)\" string$")
    public void XPath_contains_string(String word, String word2) throws Throwable {

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);


        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(obj.getProperty(word)), word2));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word) + "[text()[contains(.," + "'" + word2 + "'" + ")]]"));
        } catch (Exception e) {

            System.out.println("Ekrandaki degeder: "+ TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).getText()+"  Beklenen deger: "+ word2);
            throw new Throwable();
        }
    }


    @And("^\"([^\"]*)\" element contains to yesterdays date$")
    public void XPath_contains_to_yesterdays_date (String word) throws Throwable {

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm/DD/yyyy");
        String yesterdaysDate = dateFormat.format(now);

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(obj.getProperty(word)), yesterdaysDate));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word) + "[text()[contains(.," + "'" + yesterdaysDate + "'" + ")]]"));
        } catch (Exception e) {

            System.out.println("Ekrandaki degeder: "+ TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).getText()+"  Beklenen deger: "+ yesterdaysDate);
            throw new Throwable();
        }
    }


//     </Dynamic object based text control>


//    <Dynamic object based click>

    @And("^Click \"([^\"]*)\" css$")
    public void click_css_button(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.cssSelector(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.cssSelector(obj.getProperty(word))).click();
        } catch (Exception e) {

            throw new Throwable();
        }
    }

    @And("^Click \"([^\"]*)\" element$")
    public void click_setXPath_button(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {
            TLDriverFactory.getWait((TLDriverFactory.getTLDriver())).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).click();
        } catch (Exception e) {

            throw new Throwable();
        }
    }



    @And("^Select ([^\"]*) radio button$")
    public void click_setid_radio_button(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.id(word)));

            WebElement radioBtn1 = TLDriverFactory.getTLDriver().findElement(By.id(word));
            ((JavascriptExecutor) TLDriverFactory.getTLDriver()).executeScript("arguments[0].checked = true;", radioBtn1);

        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Click \"([^\"]*)\"$")
    public void click_setName_button(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.name(word)));

            TLDriverFactory.getTLDriver().findElement(By.name(word)).click();

        } catch (Exception e) {

            throw new Throwable();
        }

    }


    @And("^Click ([^\"]*)$")
    public void click_setID_button(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.id(word)));

            TLDriverFactory.getTLDriver().findElement(By.id(word)).click();
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Force click ([^\"]*)$")
    public void click_setID_button_force(String word) throws Throwable {

        try {
            TLDriverFactory.getTLDriver().findElement(By.id(word)).click();
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Click \"([^\"]*)\" text$")
    public void click_text_button(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.tagName(word)).click();
        } catch (Exception e) {


            throw new Throwable();
        }
    }


    @And("^Find \"([^\"]*)\" element and send return$")
    public void find_XPath_and_click(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            WebElement button = TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word)));
            button.sendKeys(Keys.RETURN);
        } catch (Exception e) {


            throw new Throwable();
        }
    }


//    </Dynamic object based click>


//    <Dynamic object based search>

    @And("^Find \"([^\"]*)\" element$")
    public void find_XPath(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word)));
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Find \"([^\"]*)\"$")
    public void find_Name(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.name(word)));

            TLDriverFactory.getTLDriver().findElement(By.name(word));
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Find ([^\"]*)$")
    public void find_Id(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.id(word)));

            TLDriverFactory.getTLDriver().findElement(By.id(word));
        } catch (Exception e) {

            throw new Throwable();
        }
    }

//    </Dynamic object based search>


//    <Dynamic object based fill with text>

    @And("^Fill \"([^\"]*)\" element with \"([^\"]*)\" text$")
    public void fill_XPath_with_text(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).sendKeys(word2);
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Fill \"([^\"]*)\" with \"([^\"]*)\" text$")
    public void fill_Name_with_text(String word, String word2) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.name(word)));

            TLDriverFactory.getTLDriver().findElement(By.name(word)).sendKeys(word2);
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Fill ([^\"]*) with \"([^\"]*)\" text$")
    public void fill_Id_with_text(String word, String word2) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.id(word)));

            TLDriverFactory.getTLDriver().findElement(By.id(word)).sendKeys(word2);

        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Fill ([^\"]*) with date$")
    public void fill_with_Date(String word) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd - HH mm ss");
        String time = dateFormat.format(now);

        try {
            TLDriverFactory.getTLDriver().findElement(By.id(word)).sendKeys(time);
        } catch (Exception e) {

            throw new PendingException();
        }
    }


    @And("^Fill \"([^\"]*)\" element with \"([^\"]*)\" text and click enter$")
    public void fill_XPath_with_text_and_click_enter(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).sendKeys((word2), Keys.ENTER);
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Fill \"([^\"]*)\" element with \"([^\"]*)\" text, click arrow down and click enter$")
    public void fill_XPath_with_text_click_arrow_down_and_click_enter(String word, String word2) throws Throwable {


        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).sendKeys((word2), Keys.ARROW_DOWN, Keys.ENTER);
        } catch (Exception e) {


            throw new Throwable();
        }

    }


//    </Dynamic object based fill with text>


//    <Dynamic object based fill with element>

    @And("^Fill \"([^\"]*)\" element with \"([^\"]*)\" element$")
    public void fill_XPath_with_element(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");

        InputStreamReader inputStreamReader = new InputStreamReader(objfile, "ISO-8859-9");

        Properties obj = new Properties();
        obj.load(inputStreamReader);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).sendKeys(obj.getProperty(word2));
        } catch (Exception e) {

            throw new Throwable();
        }
    }

    @And("^Fill and replace with yesterdays date \"([^\"]*)\" element with \"([^\"]*)\" element$")
    public void fill_and_replace_XPath_with_element(String word, String word2) throws Throwable {

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String tomorrowDate = dateFormat.format(now);

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");

        InputStreamReader inputStreamReader = new InputStreamReader(objfile, "ISO-8859-9");

        Properties obj = new Properties();
        obj.load(inputStreamReader);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).sendKeys(obj.getProperty(word2).replace("tomorrowDate",tomorrowDate));
        } catch (Exception e) {

            throw new Throwable();
        }
    }



    @And("^Fill \"([^\"]*)\" with \"([^\"]*)\" element$")
    public void fill_Name_with_element(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.name(word)));

            TLDriverFactory.getTLDriver().findElement(By.name(word)).sendKeys(obj.getProperty(word2));
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Fill ([^\"]*) with \"([^\"]*)\" element$")
    public void fill_Id_with_element(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.id(word)).sendKeys(obj.getProperty(word2));
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Fill \"([^\"]*)\" element with \"([^\"]*)\" and replace ([^\"]*) with date$")
    public void fill_element_and_replace(String word, String word2, String word3) throws Throwable {

        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd - HH mm ss");
        String time = dateFormat.format(now);

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).sendKeys(obj.getProperty(word2).replace(word3, time));
        } catch (Exception e) {

            throw new Throwable();
        }
    }


//    </Dynamic object based fill with element>


//    <Dynamic object based upload file>


    @And("^Upload to \"([^\"]*)\" field \"([^\"]*)\" file$")
    public void upload_file_to_field(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {
            WebElement uploadElement = TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word)));
            uploadElement.sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\uploadFiles\\" + obj.getProperty(word2));

        } catch (Exception e) {

            throw new Throwable();
        }
    }


//    <Dynamic object based upload file>


    @And("^Close browser$")
    public void close_Browser() throws Throwable {

        TLDriverFactory.getTLDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        try {
            TLDriverFactory.getTLDriver().quit();
//            TLDriverFactory.getTLDriver().close();
        } catch (Exception e) {

            throw new Throwable();
        }
    }

    @And("^Wait for ([^\"]*) seconds")
    public void wait(int word) throws Throwable {

        TimeUnit.SECONDS.sleep(word);
    }


    @Given("^Execute ([^\"]*) sql and check \"([^\"]*)\" string and \"([^\"]*)\" column$")
    public void given_Execute_Sql_And_Check_String_And_Coloumn(String word, String word2, String word3) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word));

        while (rs.next()) {

            String word4 = rs.getString(word3);

            if (obj.getProperty(word2).equals(word4)) {
                System.out.println();
            } else {
                System.out.println("Beklenen deger: " + obj.getProperty(word2) + "   Veritabanindan donen deger: " + word4);
                throw new Throwable();
            }
        }
        con.close();
    }


    @Given("^Execute ([^\"]*) sql and check \"([^\"]*)\" text and \"([^\"]*)\" column$")
    public void given_Execute_Sql_And_Check_text_And_Coloumn(String word, String word2, String word3) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word));

        while (rs.next()) {

            String word4 = rs.getString(word3);

            if (word2.equals(word4)) {
                System.out.println();
            } else {

                System.out.println("Beklenen deger: " + word2 + "  Veritabanindan donen deger: " + word4);

                throw new Throwable();
            }
        }
        con.close();
    }


    @Given("^Execute ([^\"]*) sql and check \"([^\"]*)\" contains \"([^\"]*)\" column$")
    public void given_Execute_Sql_And_Check_contains_Coloumn(String word, String word2, String word3) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word));

        while (rs.next()) {

            String word4 = rs.getString(word3);

            try {
                TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word2) + "[text()[contains(.," + "'" + word4.toLowerCase() + "'" + ")]]"));
            } catch (Exception e) {


                System.out.println(word4);
                throw new Throwable();
            }
        }
        con.close();
    }


    @Given("^Execute ([^\"]*) sql and go \"([^\"]*)\" url with \"([^\"]*)\"$")
    public void given_Execute_Sql_And_Go_Url(String word, String word2, String word3) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word));

        try {
            while (rs.next()) {

                String word4 = rs.getString(word3);
//                WebDriverManager.firefoxdriver().setup();
//                driver = new FirefoxDriver();
                TLDriverFactory.getTLDriver().manage().window().maximize();
                TLDriverFactory.getTLDriver().get(obj.getProperty(word2) + word4);
            }
            con.close();
        } catch (Exception e) {

            throw new Throwable();
        }
    }

    @Given("^Execute ([^\"]*) sql and go \"([^\"]*)\" url with \"([^\"]*)\" in current page$")
    public void given_Execute_Sql_And_Go_Url_in_Current_Page(String word, String word2, String word3) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word));

        try {
            while (rs.next()) {

                String word4 = rs.getString(word3);
                TLDriverFactory.getTLDriver().get(obj.getProperty(word2) + word4);
            }
            con.close();
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @Given("^Fill \"([^\"]*)\" element with \"([^\"]*)\" and execute ([^\"]*) sql$")
    public void given_Execute_Sql_And_Fill_Coloumn(String word, String word2, String word3, DataTable dt) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word3));

        while (rs.next()) {
            List<List<String>> list = dt.raw();

            String word4 = rs.getString(list.get(0).get(1));

            try {
                TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).sendKeys(obj.getProperty(word2).replace(list.get(0).get(0), word4));

            } catch (Exception e) {

                System.out.println(obj.getProperty(word2).replace(list.get(0).get(0), word4).replace(list.get(1).get(0), list.get(1).get(1)));
                throw new Throwable();
            }
        }
        con.close();
    }

    @And("^Press end$")
    public void press_end() throws InterruptedException {

        Thread.sleep(3000);
        Actions actions = new Actions(TLDriverFactory.getTLDriver());
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
    }


    @And("^Clear \"([^\"]*)\" element$")
    public void close_Browser(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));

            TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).clear();
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Accept alert$")
    public void accept_alert() throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.alertIsPresent());

            TLDriverFactory.getTLDriver().switchTo().alert().accept();
        } catch (Exception e) {

            throw new Throwable();
        }
    }

    @And("^Check \"([^\"]*)\" url$")
    public void check_Url(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);


        try {
            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.urlToBe(obj.getProperty(word)));

//            String URL = TLDriverFactory.getTLDriver().getCurrentUrl();
//            Assert.assertEquals(URL, obj.getProperty(word));
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^scroll down$")
    public void scroll_down() throws Throwable {

        JavascriptExecutor js = (JavascriptExecutor) TLDriverFactory.getTLDriver();
        Thread.sleep(3000);

        try {
            js.executeScript("window.scrollBy(0,1000)");
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^Execute \"([^\"]*)\" sql and write \"([^\"]*)\" column to \"([^\"]*)\" file$")
    public void write_db_column_to_file(String word, String word2, String word3) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
        Statement sqlStatement = con.createStatement();
        ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word));


        String filePath = System.getProperty("user.dir") + obj.getProperty(word3);
        try {
            while (rs.next()) {

                String input = rs.getString(word2);
                Utility.xmlLogger(input, filePath);
            }

        } catch (Exception e) {
            throw new Throwable();
        }
    }

    @And("^Execute \"([^\"]*)\" with \"([^\"]*)\" field text and check \"([^\"]*)\" value and \"([^\"]*)\" column$")
    public void get_field_text(String word, String word2, String word3, String word4) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);


        try {


            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(obj.getProperty(word2))));

            String fieldText = TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word2))).getText().toUpperCase();

            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://10.4.66.6:1433/MikroOdeme", "App_TestOtomasyon", "Test123*");
            Statement sqlStatement = con.createStatement();
            ResultSet rs = sqlStatement.executeQuery(obj.getProperty(word) + "'" + fieldText + "'" + "order by LastTransactionDate desc");

            while (rs.next()) {
                String word5 = rs.getString(word4);

                if ((word3).equals(word5)) {
                    System.out.println();
                } else {

                    System.out.println("Beklenen deger: " + (word3) + "   Veritabanindan donen deger: " + word5);
                    throw new Throwable();
                }
            }
        } catch (Exception e) {

            throw new Throwable();
        }
    }


    @And("^\"([^\"]*)\" element is not displayed$")
    public void element_is_not_displayed(String word) throws Throwable {


        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        List<WebElement> elements = TLDriverFactory.getTLDriver().findElements(By.xpath(obj.getProperty(word)));

        if (elements.isEmpty()) {
            System.out.println();

        } else {
            System.out.println(obj.getProperty(word) + " element is displayed!");

            throw new Throwable();

        }
    }


    @And("^\"([^\"]*)\" element is not displayed check for installmentError$")
    public void element_is_not_displayed2(String word) throws Throwable {


        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        List<WebElement> elements = TLDriverFactory.getTLDriver().findElements(By.xpath(obj.getProperty(word)));


        for (WebElement element : elements) {

            String input = element.getAttribute("style");   // Getirilen elementlerin niteligine gider

            if (input.equals("display: none;")) {
                System.out.println();

            } else {
                System.out.println(obj.getProperty(word) + " element is displayed!");

                throw new Throwable();

            }
        }
    }

    @And("^\"([^\"]*)\" element is disabled$")
    public void element_isdisabled(String word) throws Throwable {
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        List<WebElement> source = TLDriverFactory.getTLDriver().findElements(By.id(obj.getProperty(word)));

        for (WebElement aSource : source) {
            String loadCont = aSource.getAttribute("disabled");

            if (loadCont.equals("disabled")) {
                System.out.println();

            } else {
                System.out.println(word + " element is enabled!");

                throw new Throwable();

            }
        }
    }


    @And("^([^\"]*) checkbox is selected$")
    public void checkboxIsChecked(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.id((word))));
            WebElement checked = TLDriverFactory.getTLDriver().findElement(By.id(word));

            if (checked.getAttribute("checked") != null) {
                System.out.println();
            } else {
                throw new Throwable();
            }

        } catch (Exception e) {


            throw new Throwable();
        }
    }


    @And("^([^\"]*) checkbox is not selected")
    public void checkboxIsNotChecked(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.id((word))));
        } catch (Exception e) {
            System.out.println();

        }
        if (!TLDriverFactory.getTLDriver().findElement(By.id(word)).isSelected()) {
            System.out.println();
        } else {

            throw new Throwable();
        }

    }


    @And("^Force check ([^\"]*)$")
    public void force_check(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.id((word))));
            WebElement check = TLDriverFactory.getTLDriver().findElement(By.id(word));

            if (check.getAttribute("checked") == null) {
                check.click();
            }

        } catch (Exception e) {


            throw new Throwable();
        }
    }

    @And("^Force uncheck ([^\"]*)$")
    public void force_uncheck(String word) throws Throwable {

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.id((word))));
            WebElement check = TLDriverFactory.getTLDriver().findElement(By.id(word));

            if (check.getAttribute("checked") != null) {
                check.click();
            }

        } catch (Exception e) {


            throw new Throwable();
        }
    }


    @And("^\"([^\"]*)\" checkbox element is selected")
    public void checkboxElementIsChecked(String word) throws IOException {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.xpath((word))));

            if (TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).isSelected()) {
                System.out.println();
            } else {

                throw new PendingException();
            }
        } catch (Exception e) {
            System.out.println();
        }
    }


    @And("^\"([^\"]*)\" checkbox element is not selected")
    public void checkboxElementIsNotChecked(String word) throws IOException {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.xpath((word))));

            if (!TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word))).isSelected()) {
                System.out.println();
            } else {


                throw new PendingException();
            }
        } catch (Exception e) {

            System.out.println();


        }
    }


    // Attribute Check

    @And("^Get \"([^\"]*)\" attribute for \"([^\"]*)\" element and check with \"([^\"]*)\" string$")
    public void XPath_contains_string(String word, String word2, String word3) throws Throwable {

//        String Cont;

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);

        WebDriverWait wait2 = new WebDriverWait(TLDriverFactory.getTLDriver(), 30);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(obj.getProperty(word2))));

        WebElement cont = TLDriverFactory.getTLDriver().findElement(By.xpath(obj.getProperty(word2)));
        try {

            TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.attributeContains(cont, word, word3));

            cont.getAttribute(word);

            if (cont.getAttribute(word).equals(word3)) {  //  Getirilen deger ile set edilen degerin oldugunu kontrol eder

                System.out.println();

            }
        } catch (Exception e) {

            throw new Exception();
        }
    }


    @And("^Get \"([^\"]*)\" attribute for ([^\"]*) and check with \"([^\"]*)\" string$")
    public void Id_contains_string(String word, String word2, String word3) throws Throwable {


//
//        TLDriverFactory.getWait(TLDriverFactory.getTLDriver()).until(ExpectedConditions.visibilityOfElementLocated(By.id((word2) + "[text()[contains(.," + "'" + word3.toLowerCase() + "'" + ")]]")));

        String Cont;

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/deneme.properties");
        obj.load(objfile);

        Cont = TLDriverFactory.getTLDriver().findElement(By.id(word2)).getAttribute(word);


        if (Cont.equals(word3)) {  //  Getirilen deger ile set edilen degerin oldugunu kontrol eder

            System.out.println();

        } else {

            System.out.println("Hata");
            throw new Throwable();
        }
    }

    // Attribute Check

}