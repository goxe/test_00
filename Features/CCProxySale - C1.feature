@test @container1
Feature: CCProxySale - Default

#ToDo Token ID'li islem


# Happy Path - Owner Name Bos - Expire Year Bos - Expire Month Bos - CVV Bos (Basarili Islem) - Price Bos - Price AN - Installment Count Bos (Basarili) -
# Installment Count 13 - Installment Count AN - Abonelik Tarihi Bugunden Kucuk - Abonelik Tarihi Yarin (Basarili) - Abonelik Talebi Iptal


  @proxysale @happypath
  Scenario: 1. Proxy Sale

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml" element
    And Click btnSend

    And "outputField" element contains "5818 77** **** 2285" string
    And Execute ccProxySale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @proxysale
  Scenario: 2. Proxy Sale - Owner Name Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_OwnerName_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @proxysale
  Scenario: 3. Proxy Sale - Expire Year Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_ExpireYear_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00002" string
    And "outputField" element contains "tarihi ge" string

    #  Then Close browser


  @proxysale
  Scenario: 4. Proxy Sale - Expire Month Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_ExpireMonth_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00002" string
    And "outputField" element contains "tarihi ge" string

    #  Then Close browser


  @proxysale
  Scenario: 5. Proxy Sale - CVV Bos (Basarili Islem)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_Cvv_Bos" element
    And Click btnSend

    And "outputField" element contains "5818 77** **** 2285" string
    And Execute ccProxySale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @proxysale
  Scenario: 6. Proxy Sale - Price Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_Price_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @proxysale
  Scenario: 7. Proxy Sale - Price AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_Price_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @proxysale
  Scenario: 8. Proxy Sale - Installment Count Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_InstallmentCount_Bos" element
    And Click btnSend

    And "outputField" element contains "5818 77** **** 2285" string
    And Execute ccProxySale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @proxysale
  Scenario: 9. Proxy Sale - Installment Count AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_InstallmentCount_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @proxysale
  Scenario: 10. Proxy Sale - Installment Count 13

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_InstallmentCount_13" element
    And Click btnSend

    And "outputField" element contains "IVVP01001" string
    And "outputField" element contains "taksit se" string

    #  Then Close browser


  @proxysale
  Scenario: 11. Proxy Sale - Kart No Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_CardNo_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @proxysale
  Scenario: 12. Proxy Sale - Kart No AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_CardNo_AN" element
    And Click btnSend

    And "outputField" element contains "CCVC00001" string
    And "outputField" element contains "bir kart numaras" string

    #  Then Close browser


  @proxysale
  Scenario: 13. Proxy Sale - Abonelik Tarihi Bugunden Kucuk

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_SubscriberStarterDate_BugundenKucuk" element
    And Click btnSend

    And "outputField" element contains "Abonelik Tarihi G" string

    #  Then Close browser


  @proxysale
  Scenario: 14. Proxy Sale - Abonelik Tarihi Bugunden Kucuk

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_SubscriberStarterDate_BugundenKucuk" element
    And Click btnSend

    And "outputField" element contains "Abonelik Tarihi G" string
    And "outputField" element contains "failed" string


    #  Then Close browser


  @proxysale
  Scenario: 15. Proxy Sale - Abonelik Tarihi Yarin (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill and replace with yesterdays date "inputField" element with "ccProxySaleXml_SubscriberStarterDate_YarininTarihi" element
    And Click btnSend

    And "outputField" element contains "00000000-0000-0000-0000-000000000000" string
    And "outputField" element contains "5818 77** **** 2285" string

    And Execute subscriberStartDate sql and check "00000000-0000-0000-0000-000000000000" text and "OrderId" column
    And Execute subscriberStartDate sql and check "0" text and "Status" column
    And Execute subscriberStartDate sql and check "0" text and "TrxCount" column
    And "outputField" element contains "Success" string
#    And Execute ccProxySale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @proxysale
  Scenario: 16. Proxy Sale - Abonelik Talebi Iptal

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySaleXml_SubscribeIptal" and execute subscriberStartDate sql
      | SubscriptionId | ObjectId |
    And Click btnSend

    And "outputField" element contains "Success" string
    And Execute subscriberStartDate sql and check "1" text and "Status" column

    #  Then Close browser
