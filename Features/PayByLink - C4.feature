@test @container4
Feature: PayByLink

#ToDo Handle - javascript error

  @paybylink @de @ignore
  Scenario: PayByLink

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "payByLinkXml" element
    And Click btnSend

    And "outputField" element contains "Success" string

    #  Then Close browser


  @paybylink @de @ignore
  Scenario: PayByLink - Ortak Odeme Sayfasi

#    When Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"
    Given Execute addSaleRequestMailId sql and go "emailReqId" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text

    And Click installmentsselect
    And Get "value" attribute for "3TaksitKontrol" element and check with "3" string

    #  Then Close browser



  @paybylink @de @happypath
  Scenario: PayByLink

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "payByLinkXml" element
    And Click btnSend

    And "outputField" element contains "Success" string

    #  Then Close browser


  @paybylink @de @happypath
  Scenario: PayByLink - Ortak Odeme Sayfasi

#    When Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"
    Given Execute addSaleRequestMailId sql and go "emailReqId" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text


    And Fill uc_cppf_cardExpirationMonth with "12" text
    And Fill uc_cppf_cardExpirationYear with "2020" text
    And Fill uc_cppf_cardCVV with "001" text

    And Click uc_cppf_Button1

    And Check "yonlendirmeliLinkPayBy" url
    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And Check "successliLinkPayBy" url

    #  Then Close browser



  @paybysms @de @ignore
  Scenario: PayBySms

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "payBySMSXml" element
    And Click btnSend

    And "outputField" element contains "Success" string

    #  Then Close browser


  @paybysms @de @ignore
  Scenario: PayBySms - Ortak Odeme Sayfasi

#    When Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"
    Given Execute addSaleRequestMailId sql and go "emailReqId" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text

    And Fill uc_cppf_cardExpirationMonth with "12" text
    And Fill uc_cppf_cardExpirationYear with "2020" text
    And Fill uc_cppf_cardCVV with "001" text

    And Click uc_cppf_Button1

    And Check "yonlendirmeliLinkPayBy" url
    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And Check "successliLinkPayBy" url

    #  Then Close browser


  @paybyemail @de @ignore
  Scenario: PayByEmail

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "payByEmailXml" element
    And Click btnSend

    And "outputField" element contains "Success" string

    #  Then Close browser


  @paybyemail @de @ignore
  Scenario: PayByEmail - Ortak Odeme Sayfasi

#    When Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"
    Given Execute addSaleRequestMailId sql and go "emailReqId" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text

    And Fill uc_cppf_cardExpirationMonth with "12" text
    And Fill uc_cppf_cardExpirationYear with "2020" text
    And Fill uc_cppf_cardCVV with "001" text

    And Click uc_cppf_Button1

    And Check "yonlendirmeliLinkPayBy" url
    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And Check "successliLinkPayBy" url

    #  Then Close browser
