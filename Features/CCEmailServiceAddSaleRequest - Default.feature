Feature: CCEmailServiceAddSaleRequest - Default



  @ignore
  Scenario: AddSaleRequest

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "CCEmailServiceAddSaleRequestXml" element
    And Click btnSend

    And "outputField" element contains "Success" string

    And Wait for 5 seconds

    Then Close browser



  @ignore
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute addSaleRequestMailId sql and go "emailReqId" url with "Id"
    Then Close browser

    Given Execute addSaleRequestMailTicketId sql and go "wdTicketSaleUrl" url with "TicketId"

    And Wait for 2 seconds

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2020" element
    And Fill "cvv" element with "v2Cvv" element
    And Click "odemeYap" element

 #   3D Secure

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element
#    And Check "wirecardSuccess" url

    Then Close browser
