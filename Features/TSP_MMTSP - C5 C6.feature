@test @ignor
Feature: TSP&MMTSP



  @mmtsp @happypath @container4
  Scenario: 1.MMTSP

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCProxy" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "4531444531442283" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "MMTSP" text
    And Fill tbPaymentContent with "MMTSP" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text

    And Fill tbCustomerName with "goktu" text
    And Fill tbCustomerSurname with "Erdogan" text
    And Fill tbCustomerEmail with "goktu.erdogan@wirecard.com" text

    And Click btnHash

    And Click btnPost

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmtspStatus sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "58" text and "State" column

    And Execute "MM_xmlData" sql and write "Data" column to "MM_xmlFilePath" file

    #  Then Close browser



  @mmtsp @happypath @container4
  Scenario: 2.MMTSP Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to mmtspApproveXml with "MM_xmlFilePath" file

    And Click btnSend

    And Wait for 5 seconds

    And "outputField" element contains "Approved:" string

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "5" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "100" text and "State" column

    #  Then Close browser



  @tsp @happypath @container6
  Scenario: 3.TSP

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale3DSec" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "tspInputXml" element
    And Click btnSend

    And Wait for 5 seconds

    And Execute tspStatus sql and check "1" text and "CustomFlowType" column
    And Execute tspStatus sql and check "1" text and "CustomFlowState" column
    And Execute tspStatus sql and check "26" text and "TwoStepTimeout" column
    And Execute tspStatus sql and check "42" text and "State" column

    #  Then Close browser



  @tsp @happypath @container6
  Scenario: 4.Yonlendirmeli Link ve 3DSecure

    Given Execute tspStatus sql and go "ccProxySale3DSecUrl" url with "ObjectId"
    And Execute tspStatus sql and check "56" text and "State" column

#    And Wait for 2 seconds

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And Wait for 5 seconds

    And Execute tspStatus sql and check "1" text and "CustomFlowType" column
    And Execute tspStatus sql and check "2" text and "CustomFlowState" column
    And Execute tspStatus sql and check "58" text and "State" column

    #  Then Close browser



  @tsp @happypath @container6
  Scenario: 5.TSP Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "tspApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to tspApproveXml

    And Click btnSend

    And "outputField" element contains "Approved:" string

    And Execute tspStatus sql and check "1" text and "CustomFlowType" column
    And Execute tspStatus sql and check "5" text and "CustomFlowState" column
    And Execute tspStatus sql and check "100" text and "State" column

    #  Then Close browser



  @mmtspfail
  Scenario: 1.MMTSP

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCProxy" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "4531444531442283" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "MMTSP" text
    And Fill tbPaymentContent with "MMTSP" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text

    And Fill tbCustomerName with "HepsiGecerli" text
    And Fill tbCustomerSurname with "Erdogan" text
    And Fill tbCustomerEmail with "goktu.erdogan@wirecard.com" text
    And Fill tbLanguage with "TR" text

    And Click btnHash

    And Click btnPost

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmtspStatus sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "58" text and "State" column

    And Execute "MM_xmlData" sql and write "Data" column to "MM_xmlFilePath" file


  @mmtspfail
  Scenario: 2.MMTSP Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to mmtspApproveXml with "MM_xmlFilePath" file

    And Click btnSend

    And Wait for 5 seconds

    And "outputField" element contains "Approved:" string

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "5" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "100" text and "State" column

    #  Then Close browser


  @mmtspfail
  Scenario: 1.MMTSP

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCProxy" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "4531444531442283" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "MailHatali" text
    And Fill tbPaymentContent with "MMTSP" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text

    And Fill tbCustomerName with "MailHatali" text
    And Fill tbCustomerSurname with "Erdogan" text
    And Fill tbCustomerEmail with "asdasd" text
    And Fill tbLanguage with "EN" text

    And Click btnHash

    And Click btnPost

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmtspStatus sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "58" text and "State" column

    And Execute "MM_xmlData" sql and write "Data" column to "MM_xmlFilePath" file


  @mmtspfail
  Scenario: 1.MMTSP

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCProxy" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "4531444531442283" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "MMTSP" text
    And Fill tbPaymentContent with "MMTSP" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text

    And Fill tbCustomerName with "DilHatali" text
    And Fill tbCustomerSurname with "Erdogan" text
    And Fill tbCustomerEmail with "goktu.erdogan@wirecard.com" text
    And Fill tbLanguage with "asd" text

    And Click btnHash

    And Click btnPost

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmtspStatus sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "58" text and "State" column

    And Execute "MM_xmlData" sql and write "Data" column to "MM_xmlFilePath" file


  @mmtspfail
  Scenario: 2.MMTSP Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to mmtspApproveXml with "MM_xmlFilePath" file

    And Click btnSend

    And Wait for 5 seconds

    And "outputField" element contains "Approved:" string

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "5" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "100" text and "State" column

    #  Then Close browser


  @mmtspfail
  Scenario: 1.MMTSP

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCProxy" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "4531444531442283" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "MMTSP" text
    And Fill tbPaymentContent with "MMTSP" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text

    And Fill tbCustomerName with "AdDoluSoyadBos" text
    And Fill tbCustomerSurname with "" text
    And Fill tbCustomerEmail with "goktu.erdogan@wirecard.com" text
    And Fill tbLanguage with "TR" text

    And Click btnHash

    And Click btnPost

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmtspStatus sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "58" text and "State" column

    And Execute "MM_xmlData" sql and write "Data" column to "MM_xmlFilePath" file


  @mmtspfail
  Scenario: 2.MMTSP Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to mmtspApproveXml with "MM_xmlFilePath" file

    And Click btnSend

    And Wait for 5 seconds

    And "outputField" element contains "Approved:" string

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "5" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "100" text and "State" column

    #  Then Close browser


  @mmtspfail
  Scenario: 1.MMTSP

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCProxy" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "4531444531442283" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "MMTSP" text
    And Fill tbPaymentContent with "MMTSP" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text

    And Fill tbCustomerName with "" text
    And Fill tbCustomerSurname with "HepsiBosDilTR" text
    And Fill tbCustomerEmail with "" text
    And Fill tbLanguage with "TR" text

    And Click btnHash

    And Click btnPost

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmtspStatus sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 5 seconds

    And Execute mmtspStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmtspStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmtspStatus sql and check "58" text and "State" column

    And Execute "MM_xmlData" sql and write "Data" column to "MM_xmlFilePath" file