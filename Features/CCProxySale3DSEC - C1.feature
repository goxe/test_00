@test @container
Feature: CCProxySale3DSEC - Default


# Happy Path - Owner Name Bos - Expire Year Bos - Expire Month Bos - CVV Bos (Basarili Islem) - Price Bos - Price AN - Installment Count Bos (Basarili) -
# Installment Count 13 - Installment Count AN


  @sale3dsec @happypath
  Scenario: 1. Proxy Sale3DSEC

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale3DSec" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml" element
    And Click btnSend

    And "outputField" element contains "order=" string

    #  Then Close browser


  @sale3dsec @happypath
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccProxySale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    #  Then Close browser


  @sale3dsec
  Scenario: 2. Proxy Sale3DSEC - Owner Name Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_OwnerName_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @sale3dsec
  Scenario: 3. Proxy Sale3DSEC - Expire Year Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_ExpireYear_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00002" string
    And "outputField" element contains "tarihi ge" string

    #  Then Close browser


  @sale3dsec
  Scenario: 4. Proxy Sale3DSEC - Expire Month Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_ExpireMonth_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00002" string
    And "outputField" element contains "tarihi ge" string

    #  Then Close browser


  @sale3dsec
  Scenario: 5. Proxy Sale3DSEC - CVV Bos (Basarili Islem)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_Cvv_Bos" element
    And Click btnSend

    And "outputField" element contains "5818 77** **** 2285" string

    #  Then Close browser


  @sale3dsec
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccProxySale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    #  Then Close browser


  @sale3dsec
  Scenario: 6. Proxy Sale3DSEC - Price Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_Price_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @sale3dsec
  Scenario: 7. Proxy Sale3DSEC - Price AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_Price_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @sale3dsec
  Scenario: 8. Proxy Sale3DSEC - Installment Count Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_InstallmentCount_Bos" element
    And Click btnSend

    And "outputField" element contains "5818 77** **** 2285" string

    #  Then Close browser


  @sale3dsec
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccProxySale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    #  Then Close browser


  @sale3dsec
  Scenario: 9. Proxy Sale3DSEC - Installment Count AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_InstallmentCount_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @sale3dsec
  Scenario: 10. Proxy Sale3DSEC - Installment Count 13

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_InstallmentCount_13" element
    And Click btnSend

    And "outputField" element contains "IVVP01001" string
    And "outputField" element contains "taksit se" string

    #  Then Close browser


  @sale3dsec
  Scenario: 11. Proxy Sale3DSEC - Kart No Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_CardNo_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @sale3dsec
  Scenario: 12. Proxy Sale3DSEC - Kart No AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccProxySale3DSecXml_CardNo_AN" element
    And Click btnSend

    And "outputField" element contains "CCVC00001" string
    And "outputField" element contains "bir kart numaras" string

    #  Then Close browser