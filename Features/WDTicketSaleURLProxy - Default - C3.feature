@test @container3
Feature: WDTicketSaleURLProxy - Default

# Happy Path - Price Bos - Price AN - MPAY Bos (Basarili) - Description Bos (Basarili) - Payment Content Bos


  @wdticketsaleurl @happypath @wdhappy
  Scenario: WDTicketSaleURLProxy

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    # Then Close browser


  @wdticketsaleurl @happypath @wdhappy
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "5818" text
    And Fill "kkNo" element with "7758" text
    And Fill "kkNo" element with "1877" text
    And Fill "kkNo" element with "2285" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "odemeYap" element

    And Check "pGate" url

    And Execute wdTicketSale_Sql sql and check "100" text and "State" column

    # Then Close browser


  @wdticketsaleurl @ignore
  Scenario: WDTicketSaleURLProxy - Price Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_Price_Bos" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    # Then Close browser


  @wdticketsaleurl @ignore
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "5818" text
    And Fill "kkNo" element with "7758" text
    And Fill "kkNo" element with "1877" text
    And Fill "kkNo" element with "2285" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "odemeYap" element

    And Check "pGate" url

    And Execute wdTicketSale_Sql sql and check "100" text and "State" column

    # Then Close browser


  @wdticketsaleurl @wdfail
  Scenario: WDTicketSaleURLProxy - Price AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_Price_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    # Then Close browser


  @wdticketsaleurl @wdhappy
  Scenario: WDTicketSaleURLProxy - MPAY Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_MPAY_Bos" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    # Then Close browser


  @wdticketsaleurl @wdhappy
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "5818" text
    And Fill "kkNo" element with "7758" text
    And Fill "kkNo" element with "1877" text
    And Fill "kkNo" element with "2285" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "odemeYap" element

    And Check "pGate" url

    And Execute wdTicketSale_Sql sql and check "100" text and "State" column

    # Then Close browser


  @wdticketsaleurl @wdhappy
  Scenario: WDTicketSaleURLProxy - Description Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_Description_Bos" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    # Then Close browser


  @wdticketsaleurl @wdhappy
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSaleUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "5818" text
    And Fill "kkNo" element with "7758" text
    And Fill "kkNo" element with "1877" text
    And Fill "kkNo" element with "2285" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "odemeYap" element

    And Check "pGate" url

    And Execute wdTicketSale_Sql sql and check "100" text and "State" column

#    And Execute "wdTicketSale_Sql" with "orderIdTestPage" field text and check "100" value and "State" column

    # Then Close browser


  @wdticketsaleurl @wdfail
  Scenario: WDTicketSaleURLProxy - Payment Content Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_PaymentContent_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    # Then Close browser