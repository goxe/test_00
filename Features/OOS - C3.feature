@test @container3
Feature: Ortak Odeme Sayfasi


# 3 - 6 - 9 - 12 taksit ve pesin odeme senaryolarini icerir.

  @oos
  Scenario: WDTicketSaleURLProxy - 3 Taksit

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_OOS_3Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @oos
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute OOS_Ticket sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click installmentsselect
    And Click "taksitSec1" element
    And Click "odemeYap" element

 #   3D Secure
    And Check "pGate" url

    And Execute OOS_Sql sql and check "100" text and "State" column
    And "taksit3" element contains "3" string

    #  Then Close browser


  @oos
  Scenario: WDTicketSaleURLProxy - 6 Taksit

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_OOS_6Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @oos
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute OOS_Ticket sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click installmentsselect
    And Click "taksitSec1" element
    And Click "odemeYap" element

 #   3D Secure
    And Check "pGate" url

    And Execute OOS_Sql sql and check "100" text and "State" column

    And "taksit3" element contains "6" string

    #  Then Close browser


  @oos
  Scenario: WDTicketSaleURLProxy - 9 Taksit

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_OOS_9Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @oos
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute OOS_Ticket sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click installmentsselect
    And Click "taksitSec1" element
    And Click "odemeYap" element

 #   3D Secure
    And Check "pGate" url

    And Execute OOS_Sql sql and check "100" text and "State" column
    And "taksit3" element contains "9" string

    #  Then Close browser


  @oos @ignore
  Scenario: WDTicketSaleURLProxy - 12 Taksit

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_OOS_12Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @oos @ignore
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute OOS_Ticket sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click installmentsselect
    And Click "taksitSec1" element
    And Click "odemeYap" element

 #   3D Secure
    And Check "pGate" url

    And Execute OOS_Sql sql and check "100" text and "State" column
    And "taksit3" element contains "12" string

    #  Then Close browser



  @oos
  Scenario: WDTicketSaleURLProxy - 0 Taksit (Pesin)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "WDTicketSaleURLProxyXml_OOS_0Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @oos
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute OOS_Ticket sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click installmentsselect
    And Click "taksitSec1" element
    And Click "odemeYap" element

 #   3D Secure
    And Check "pGate" url

    And Execute OOS_Sql sql and check "100" text and "State" column

#  And Execute "OOS_Sql" with "orderIdTestPage" field text and check "100" value and "State" column
    And "taksit3" element contains "0" string

    #  Then Close browser