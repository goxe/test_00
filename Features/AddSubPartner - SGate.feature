@test @ignor
Feature: AddSubPartner Uzaktan - SGate


  @addsubpartnerws @de @ignore
  Scenario: Switch Yuzyuze - Uzaktan

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Click "uyeSirketler" element
    And Fill "uyeSirketlerSirketId" element with "12356" text
    And Click ctl00_cphSolMenu_btnAra
    And Click "uyeSirketlerIsyeriBilgileriniDuzenle" element
    And Select ctl00_MainContent_rbtremote radio button
    And Find "dogrulamaKaydet" element and send return
    And Wait for 3 seconds
    And ctl00_MainContent_rbtremote checkbox is selected

    Then Close browser



  @addsubpartnerws @de @ignor @google
  Scenario: Switch Yuzyuze - Uzaktan
    When Open the Firefox and go to google


  @addsubpartnerws @de @ignore
  Scenario: SubPartner Risk Mail Gonderimi Ac

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Go programAyarlariAdvancedKullanicilarGoktugDuzenleUrl url
#    And Click "programAyarlariAdvanced" element
#    And Click "programAyarlariAdvancedKullanicilar" element
#    And Click "asdasd" css
#    And Wait for 3 seconds
#    And Click "programAyarlariAdvancedKullanicilarGoktug" element
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And Force check ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName
    And Click ctl00_MainContent_btnSubmit
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName checkbox is selected

    Then Close browser



  @addsubpartnerws
  Scenario: AddSubPartner - Individual Zorunlu

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivZorunluDataXml" and replace subPartnerUniqueID with date
#    And Click btnSend
#
#    And "outputField" element contains "https://test.wirecard.com.tr/pgate/tr/subpartner/agreement?ticket" string
#
#    Then Close browser
#
#    And Execute subPartnerStatus sql and check "WAITINGFORAGREEMENT" text and "ConfirmationStatus" column
#
#    Given Execute wdTicketSale3DSUrlProxyId sql and go "subPartnerAgreement" url with "Id"
#    And Press end
#
#    And Click termcheck
#    And Click btnConfirm
#
#    And Click "uyeIsyeriOnayUlasti" element
#
#    And Check "onayWD" url
#
#    And Execute subPartnerStatus sql and check "CONFIRMED" text and "ConfirmationStatus" column
#
#    Then Close browser


  @addsubpartnerws @ignore
  Scenario: Pending Onay
    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Go subPartnerPending url

    And Click "subPartnerPendingDetay" element
#    And Wait for 5 seconds
    And Press end
    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

#    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Individual Zorunlu Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivZorunluDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Individual Tam Data

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivTamDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "https://test.wirecard.com.tr/pgate/tr/subpartner/agreement?ticket" string

    Then Close browser

    And Execute subPartnerStatus sql and check "WAITINGFORAGREEMENT" text and "ConfirmationStatus" column

    Given Execute wdTicketSale3DSUrlProxyId sql and go "subPartnerAgreement" url with "Id"
    And Press end

    And Click termcheck
    And Click btnConfirm

    And Click "uyeIsyeriOnayUlasti" element

    And Check "onayWD" url

    And Execute subPartnerStatus sql and check "CONFIRMED" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @ignore
  Scenario: Pending Onay
    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Go subPartnerPending url

    And Click "subPartnerPendingDetay" element
    And Press end

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    And Execute subPartnerStatus sql and check "CONFIRMED" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Individual Tam Data Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivTamDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Private Zorunlu

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateZorunluDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "https://test.wirecard.com.tr/pgate/tr/subpartner/agreement?ticket" string

    Then Close browser

    And Execute subPartnerStatus sql and check "WAITINGFORAGREEMENT" text and "ConfirmationStatus" column

    Given Execute wdTicketSale3DSUrlProxyId sql and go "subPartnerAgreement" url with "Id"
    And Press end

    And Click termcheck
    And Click btnConfirm

    And Click "uyeIsyeriOnayUlasti" element

    And Check "onayWD" url

    And Execute subPartnerStatus sql and check "PENDINGFORRISK" text and "ConfirmationStatus" column


    Then Close browser


  @addsubpartnerws @de
  Scenario: Pending Onay
    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Go subPartnerPending url

    And Click "subPartnerPendingDetay" element
    And Press end

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    And Execute subPartnerStatus sql and check "CONFIRMED" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Private Zorunlu Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateZorunluDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Private Tam Data

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateTamDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "https://test.wirecard.com.tr/pgate/tr/subpartner/agreement?ticket" string

    Then Close browser

    And Execute subPartnerStatus sql and check "WAITINGFORAGREEMENT" text and "ConfirmationStatus" column

    Given Execute wdTicketSale3DSUrlProxyId sql and go "subPartnerAgreement" url with "Id"
    And Press end

    And Click termcheck
    And Click btnConfirm

    And Click "uyeIsyeriOnayUlasti" element

    And Check "onayWD" url

    And Execute subPartnerStatus sql and check "PENDINGFORRISK" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: Pending Onay
    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Go subPartnerPending url

    And Click "subPartnerPendingDetay" element
    And Press end

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    And Execute subPartnerStatus sql and check "CONFIRMED" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Private Tam Data Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateTamDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Corporation Zorunlu

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpZorunluDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "https://test.wirecard.com.tr/pgate/tr/subpartner/agreement?ticket" string

    Then Close browser

    And Execute subPartnerStatus sql and check "WAITINGFORAGREEMENT" text and "ConfirmationStatus" column

    Given Execute wdTicketSale3DSUrlProxyId sql and go "subPartnerAgreement" url with "Id"
    And Press end

    And Click termcheck
    And Click btnConfirm

    And Click "uyeIsyeriOnayUlasti" element

    And Check "onayWD" url

    And Execute subPartnerStatus sql and check "PENDINGFORRISK" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: Pending Onay
    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Go subPartnerPending url

    And Click "subPartnerPendingDetay" element
    And Press end

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    And Execute subPartnerStatus sql and check "CONFIRMED" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Corporation Zorunlu Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpZorunluDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Corporation Tam Data

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpTamDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "https://test.wirecard.com.tr/pgate/tr/subpartner/agreement?ticket" string

    Then Close browser

    And Execute subPartnerStatus sql and check "WAITINGFORAGREEMENT" text and "ConfirmationStatus" column

    Given Execute wdTicketSale3DSUrlProxyId sql and go "subPartnerAgreement" url with "Id"
    And Press end

    And Click termcheck
    And Click btnConfirm

    And Click "uyeIsyeriOnayUlasti" element

    And Check "onayWD" url

    And Execute subPartnerStatus sql and check "PENDINGFORRISK" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: Pending Onay
    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123**" text
    And Click cbpLogin_btnLogin

    And Go subPartnerPending url

    And Click "subPartnerPendingDetay" element
    And Press end

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    And Execute subPartnerStatus sql and check "CONFIRMED" text and "ConfirmationStatus" column

    Then Close browser


  @addsubpartnerws @de
  Scenario: AddSubPartner - Corporation Tam Data Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpTamDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser
    
