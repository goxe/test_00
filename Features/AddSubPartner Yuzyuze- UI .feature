@test @ignore
Feature: AddSubPartner Yuzyuze - UI


  @addsubpartneruiyy @de @ignor
  Scenario: SubPartner Risk Mail Gonderimi Kapat

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go programAyarlariAdvancedKullanicilarGoktugDuzenleUrl url
#    And Click "programAyarlariAdvanced" element
#    And Click "programAyarlariAdvancedKullanicilar" element
#    And Click "asdasd" css
#    And Wait for 3 seconds
#    And Click "programAyarlariAdvancedKullanicilarGoktug" element
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And Force check ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName
    And Click ctl00_MainContent_btnSubmit
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName checkbox is not selected

    Then Close browser


  @addsubpartneruiyy @de @ignor
  Scenario: Switch Uzaktan - Yuzyuze

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Click "uyeSirketler" element
    And Fill "uyeSirketlerSirketId" element with "12356" text
    And Click ctl00_cphSolMenu_btnAra
    And Click "uyeSirketlerIsyeriBilgileriniDuzenle" element
    And Select ctl00_MainContent_rbtftf radio button
    And Find "dogrulamaKaydet" element and send return
    And ctl00_MainContent_rbtftf checkbox is selected

    Then Close browser



  @addsubpartneruiyy @de
  Scenario: AddSubPartner - Individual Tam Data

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go subPartnerAddUI url

    And Fill ctl00_MainContent_ctrlName with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBranchName with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlPartnerId with "12356" text
    And Fill ctl00_MainContent_ctrlUniqueId with date
    And Fill ctl00_MainContent_ctrlSubPartnerTypeId with "Individual" text

    And Fill ctl00_MainContent_ctrlCountry with "TR" text
    And Fill ctl00_MainContent_ctrlCity with "Istanbul" text
    And Fill ctl00_MainContent_ctrlAddress with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBusinessPhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlMobilePhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlInvoiceEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlIdentityNumber with "12744365944" text
    And Fill ctl00_MainContent_ctrlAuthName with "DummyName" text
    And Fill ctl00_MainContent_ctrlAuthSurname with "DummySurname" text

#    And Fill ctl00_MainContent_ctrlAuthBirthDate with "1900/01/01" text
    And Fill ctl00_MainContent_ctrlTaxOffice with "Sisli" text

    And Fill ctl00_MainContent_ctrlTaxNumber with "1234567890" text

    And Fill ctl00_MainContent_ctrlBankName with "0208" text

    And Fill ctl00_MainContent_ctrlIBAN with "TR000000000000000000000001" text

    And Fill ctl00_MainContent_ctrlTradeRegisterNumber with "32214323" text

    And Fill ctl00_MainContent_ctrlTradeChamber with "2342342" text
    And Fill ctl00_MainContent_ctrlActive with "Aktif" text

    And Click ctl00_MainContent_btnSave
    And Click ctl00_MainContent_btnConfirm
    And Click "yuzyuzeOnay" element

    Then Close browser



  @addsubpartneruiyy @de
  Scenario: AddSubPartner - Private Company Tam Data

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go subPartnerAddUI url

    And Fill ctl00_MainContent_ctrlName with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBranchName with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlPartnerId with "12356" text
    And Fill ctl00_MainContent_ctrlUniqueId with date
    And Fill ctl00_MainContent_ctrlSubPartnerTypeId with "Private Company" text

    And Fill ctl00_MainContent_ctrlCountry with "TR" text
    And Fill ctl00_MainContent_ctrlCity with "Istanbul" text
    And Fill ctl00_MainContent_ctrlAddress with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBusinessPhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlMobilePhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlInvoiceEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlIdentityNumber with "12744365944" text
    And Fill ctl00_MainContent_ctrlAuthName with "DummyName" text
    And Fill ctl00_MainContent_ctrlAuthSurname with "DummySurname" text

#    And Fill ctl00_MainContent_ctrlAuthBirthDate with "1900/01/01" text
    And Fill ctl00_MainContent_ctrlTaxOffice with "Sisli" text

    And Fill ctl00_MainContent_ctrlTaxNumber with "1234567890" text

    And Fill ctl00_MainContent_ctrlBankName with "0208" text

    And Fill ctl00_MainContent_ctrlIBAN with "TR000000000000000000000001" text

    And Fill ctl00_MainContent_ctrlTradeRegisterNumber with "32214323" text

    And Fill ctl00_MainContent_ctrlTradeChamber with "2342342" text
    And Fill ctl00_MainContent_ctrlActive with "Aktif" text

    And Click ctl00_MainContent_btnSave
    And Click ctl00_MainContent_btnConfirm
    And Click "yuzyuzeOnay" element

    Then Close browser



  @addsubpartneruiyy @de
  Scenario: AddSubPartner - Corporation Tam Data

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go subPartnerAddUI url

    And Fill ctl00_MainContent_ctrlName with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBranchName with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlPartnerId with "12356" text
    And Fill ctl00_MainContent_ctrlUniqueId with date
    And Fill ctl00_MainContent_ctrlSubPartnerTypeId with "Corporation" text

    And Fill ctl00_MainContent_ctrlCountry with "TR" text
    And Fill ctl00_MainContent_ctrlCity with "Istanbul" text
    And Fill ctl00_MainContent_ctrlAddress with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBusinessPhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlMobilePhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlInvoiceEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlIdentityNumber with "12744365944" text
    And Fill ctl00_MainContent_ctrlAuthName with "DummyName" text
    And Fill ctl00_MainContent_ctrlAuthSurname with "DummySurname" text

#    And Fill ctl00_MainContent_ctrlAuthBirthDate with "1900/01/01" text
    And Fill ctl00_MainContent_ctrlTaxOffice with "Sisli" text

    And Fill ctl00_MainContent_ctrlTaxNumber with "1234567890" text

    And Fill ctl00_MainContent_ctrlBankName with "0208" text

    And Fill ctl00_MainContent_ctrlIBAN with "TR000000000000000000000001" text

    And Fill ctl00_MainContent_ctrlTradeRegisterNumber with "32214323" text

    And Fill ctl00_MainContent_ctrlTradeChamber with "2342342" text
    And Fill ctl00_MainContent_ctrlActive with "Aktif" text

    And Click ctl00_MainContent_btnSave
    And Click ctl00_MainContent_btnConfirm
    And Click "yuzyuzeOnay" element

    Then Close browser



  @addsubpartneruiyy @de
  Scenario: AddSubPartner - Individual Zorunlu

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go subPartnerAddUI url

    And Fill ctl00_MainContent_ctrlName with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBranchName with "" text
    And Fill ctl00_MainContent_ctrlPartnerId with "12356" text
    And Fill ctl00_MainContent_ctrlUniqueId with date
    And Fill ctl00_MainContent_ctrlSubPartnerTypeId with "Individual" text

    And Fill ctl00_MainContent_ctrlCountry with "TR" text
    And Fill ctl00_MainContent_ctrlCity with "Istanbul" text
    And Fill ctl00_MainContent_ctrlAddress with "OTOMASYON" text
#    And Fill ctl00_MainContent_ctrlBusinessPhone with "" text
    And Fill ctl00_MainContent_ctrlMobilePhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlEmail with "otomasyon@wirecard.com" text
#    And Fill ctl00_MainContent_ctrlInvoiceEmail with "" text
    And Fill ctl00_MainContent_ctrlIdentityNumber with "12744365944" text
    And Fill ctl00_MainContent_ctrlAuthName with "DummyName" text
    And Fill ctl00_MainContent_ctrlAuthSurname with "DummySurname" text

#    And Fill ctl00_MainContent_ctrlAuthBirthDate with "1900/01/01" text
#    And Fill ctl00_MainContent_ctrlTaxOffice with "" text
#
#    And Fill ctl00_MainContent_ctrlTaxNumber with "" text
#
    And Fill ctl00_MainContent_ctrlBankName with "0208" text

    And Fill ctl00_MainContent_ctrlIBAN with "TR000000000000000000000001" text

#    And Fill ctl00_MainContent_ctrlTradeRegisterNumber with "" text
#
#    And Fill ctl00_MainContent_ctrlTradeChamber with "" text
    And Fill ctl00_MainContent_ctrlActive with "Aktif" text

    And Click ctl00_MainContent_btnSave
    And Click ctl00_MainContent_btnConfirm
    And Click "yuzyuzeOnay" element

    Then Close browser



  @addsubpartneruiyy @de
  Scenario: AddSubPartner - Private Company Zorunlu

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go subPartnerAddUI url

    And Fill ctl00_MainContent_ctrlName with "OTOMASYON" text
#    And Fill ctl00_MainContent_ctrlBranchName with "" text
    And Fill ctl00_MainContent_ctrlPartnerId with "12356" text
    And Fill ctl00_MainContent_ctrlUniqueId with date
    And Fill ctl00_MainContent_ctrlSubPartnerTypeId with "Private Company" text

    And Fill ctl00_MainContent_ctrlCountry with "TR" text
    And Fill ctl00_MainContent_ctrlCity with "Istanbul" text
    And Fill ctl00_MainContent_ctrlAddress with "OTOMASYON" text
#    And Fill ctl00_MainContent_ctrlBusinessPhone with "" text
    And Fill ctl00_MainContent_ctrlMobilePhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlEmail with "otomasyon@wirecard.com" text
#    And Fill ctl00_MainContent_ctrlInvoiceEmail with "" text
    And Fill ctl00_MainContent_ctrlIdentityNumber with "12744365944" text
    And Fill ctl00_MainContent_ctrlAuthName with "DummyName" text
    And Fill ctl00_MainContent_ctrlAuthSurname with "DummySurname" text

#    And Fill ctl00_MainContent_ctrlAuthBirthDate with "1900/01/01" text
    And Fill ctl00_MainContent_ctrlTaxOffice with "Sisli" text

    And Fill ctl00_MainContent_ctrlTaxNumber with "1234567890" text

    And Fill ctl00_MainContent_ctrlBankName with "0208" text

    And Fill ctl00_MainContent_ctrlIBAN with "TR000000000000000000000001" text

#    And Fill ctl00_MainContent_ctrlTradeRegisterNumber with "" text
#
#    And Fill ctl00_MainContent_ctrlTradeChamber with "" text
    And Fill ctl00_MainContent_ctrlActive with "Aktif" text

    And Click ctl00_MainContent_btnSave
    And Click ctl00_MainContent_btnConfirm
    And Click "yuzyuzeOnay" element

    Then Close browser



  @addsubpartneruiyy @de
  Scenario: AddSubPartner - Corporation Zorunlu

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go subPartnerAddUI url

    And Fill ctl00_MainContent_ctrlName with "OTOMASYON" text
#    And Fill ctl00_MainContent_ctrlBranchName with "" text
    And Fill ctl00_MainContent_ctrlPartnerId with "12356" text
    And Fill ctl00_MainContent_ctrlUniqueId with date
    And Fill ctl00_MainContent_ctrlSubPartnerTypeId with "Corporation" text

    And Fill ctl00_MainContent_ctrlCountry with "TR" text
    And Fill ctl00_MainContent_ctrlCity with "Istanbul" text
    And Fill ctl00_MainContent_ctrlAddress with "OTOMASYON" text
    And Fill ctl00_MainContent_ctrlBusinessPhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlMobilePhone with "5555555555" text
    And Fill ctl00_MainContent_ctrlEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlInvoiceEmail with "otomasyon@wirecard.com" text
    And Fill ctl00_MainContent_ctrlIdentityNumber with "12744365944" text
    And Fill ctl00_MainContent_ctrlAuthName with "DummyName" text
    And Fill ctl00_MainContent_ctrlAuthSurname with "DummySurname" text

#    And Fill ctl00_MainContent_ctrlAuthBirthDate with "1900/01/01" text
    And Fill ctl00_MainContent_ctrlTaxOffice with "Sisli" text

    And Fill ctl00_MainContent_ctrlTaxNumber with "1234567890" text

    And Fill ctl00_MainContent_ctrlBankName with "0208" text

    And Fill ctl00_MainContent_ctrlIBAN with "TR000000000000000000000001" text

    And Fill ctl00_MainContent_ctrlTradeRegisterNumber with "32214323" text

    And Fill ctl00_MainContent_ctrlTradeChamber with "2342342" text
    And Fill ctl00_MainContent_ctrlActive with "Aktif" text

    And Click ctl00_MainContent_btnSave
    And Click ctl00_MainContent_btnConfirm
    And Click "yuzyuzeOnay" element

    Then Close browser


  @addsubpartneruiyy @de
  Scenario: Switch Yuzyuze - Uzaktan

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Click "uyeSirketler" element
    And Fill "uyeSirketlerSirketId" element with "12356" text
    And Click ctl00_cphSolMenu_btnAra
    And Click "uyeSirketlerIsyeriBilgileriniDuzenle" element
    And Select ctl00_MainContent_rbtremote radio button
    And Find "dogrulamaKaydet" element and send return
    And Wait for 3 seconds
    And ctl00_MainContent_rbtremote checkbox is selected

    Then Close browser


  @addsubpartneruiyy @de
  Scenario: SubPartner Risk Mail Gonderimi Ac

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go programAyarlariAdvancedKullanicilarGoktugDuzenleUrl url
#    And Click "programAyarlariAdvanced" element
#    And Click "programAyarlariAdvancedKullanicilar" element
#    And Click "asdasd" css
#    And Wait for 3 seconds
#    And Click "programAyarlariAdvancedKullanicilarGoktug" element
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And Click ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName
    And Click ctl00_MainContent_btnSubmit
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName checkbox is selected

    Then Close browser

