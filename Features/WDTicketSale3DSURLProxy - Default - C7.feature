@test
Feature: WDTicketSale3DSURLProxy - Default

#  Happy Path - Taksitli Islem (0&3&6) - Taksit Yetkisi Olmayan - Taksit Yetkisi Olmayan (Taksit Hatasi Alan Kontrolu)-  Price Bos - Price AN - MPAY Bos (Basarili) - Description Bos (Basarili) - Payment Content Bos

  @wdTicketSale3DSec @happypath @wdhappy
  Scenario: WDTicketSale3DSURLProxy

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSale3DSUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser



  @wdTicketSale3DSec @happypath @wdhappy
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSale3DSUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "odemeYap" element

 #   3D Secure

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    And Execute wdTicketSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser



  @wdTicketSale3DSec @wdhappy
  Scenario: WDTicketSale3DSURLProxy Taksitli Islem (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSale3DSUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_0_3_6_Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser



  @wdTicketSale3DSec @wdhappy
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSale3DSUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "3Taksit" element
    And Click "odemeYap" element

 #   3D Secure

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    And Execute wdTicketSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser



  @wdTicketSale3DSec @wdfail
  Scenario: WDTicketSale3DSURLProxy Taksit Yetkisi Olmayan

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSale3DSUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_0_3_6_Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser



  @wdTicketSale3DSec @wdfail
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSale3DSUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4444" text
    And Fill "kkNo" element with "4444" text
    And Fill "kkNo" element with "4444" text
    And Fill "kkNo" element with "4444" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "v1Cvv" element

    And "installmentslist" element is not displayed
    And "installmentError" element is not displayed check for installmentError

    #  Then Close browser



  @wdTicketSale3DSec @wdfail
  Scenario: WDTicketSale3DSURLProxy Taksit Yetkisi Olmayan (Taksit Hatasi Alan Kontrolu)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSale3DSUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_9_Taksit" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser



  @wdTicketSale3DSec @wdfail
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSale3DSUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4444" text
    And Fill "kkNo" element with "4444" text
    And Fill "kkNo" element with "4444" text
    And Fill "kkNo" element with "4444" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "v1Cvv" element

    And "installmentslist" element is not displayed
    And Find "installmentError" element

    #  Then Close browser


  @wdTicketSale3DSec @ignore
  Scenario: WDTicketSale3DSURLProxy - Price Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_Price_Bos" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @wdTicketSale3DSec @ignore
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSale3DSUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "3Taksit" element
    And Click "odemeYap" element

 #   3D Secure

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    And Execute wdTicketSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @wdTicketSale3DSec @wdfail
  Scenario: WDTicketSale3DSURLProxy - Price AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_Price_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @wdTicketSale3DSec @wdhappy
  Scenario: WDTicketSale3DSURLProxy - MPAY Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_MPAY_Bos" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @wdTicketSale3DSec @wdhappy
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSale3DSUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "3Taksit" element
    And Click "odemeYap" element

 #   3D Secure

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    And Execute wdTicketSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @wdTicketSale3DSec @wdhappy
  Scenario: WDTicketSale3DSURLProxy - Description Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_Description_Bos" element
    And Click btnSend

    And "outputField" element contains "Basarili" string

    #  Then Close browser


  @wdTicketSale3DSec @wdhappy
  Scenario: Yonlendirmeli Link, Kart Bilgileri ve 3DSecure

    Given Execute wdTicketSale3DSUrlProxyId sql and go "wdTicketSaleUrl" url with "Id"

    And Fill "kartUzerindekiIsim" element with "TestCard" text
    And Wait for 1 seconds
    And Fill "kkNo" element with "4531" text
    And Fill "kkNo" element with "4445" text
    And Fill "kkNo" element with "3144" text
    And Fill "kkNo" element with "2283" text
    And Wait for 1 seconds
    And Click "sonKullanmaAy" element
    And Click "ay12" element
    And Click "sonKullanmaYil" element
    And Click "yil2018" element
    And Fill "cvv" element with "001" text
    And Click "3Taksit" element
    And Click "odemeYap" element

 #   3D Secure

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string

    And Execute wdTicketSale3DSEC_Sql sql and check "100" text and "State" column

#    And Execute "wdTicketSale3DSEC_Sql" with "orderIdTestPage" field text and check "100" value and "State" column

    #  Then Close browser



  @wdTicketSale3DSec @wdfail
  Scenario: WDTicketSale3DSURLProxy - Payment Content Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "wdTicketSaleUrlProxy" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "wdTicketSale3DSUrlProxyXml_PaymentContent_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser