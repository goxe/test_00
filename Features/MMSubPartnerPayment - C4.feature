@test @container5
Feature: MMSubPartnerPayment



@mmsubppartnerpayment @de @happypath
Scenario: 1.SubPartnerPayment

  When Open the Firefox and go to mmtspUrl

  And Fill tbServiceType with "CCMarketPlace" text
  And Fill tbOpertaionType with "Sale3DSEC" text
  And Fill tbUserCode with "20104" text
  And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
  And Fill tbPrice with "100" text
  And Fill tbOwnerName with "otomasyon" text
  And Fill tbCardNo with "5818775818772285" text
  And Fill tbExpireYear with "20" text
  And Fill tbExpireMonth with "12" text
  And Fill tbCvv with "001" text
  And Fill tbInstallmentCount with "0" text
  And Fill tbMPAY with "Otomasyon" text
  And Fill tbPaymentContent with "MMSUBPARTNERPAYMENT" text
  And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
  And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
  And Fill tbCurrency with "TRY" text
  And Fill tbPosLang with "tr" text
  And Fill tbHashRandomParam with "123456" text

  And Fill tbCustFlowType with "2" text
  And Fill tbTwoStepTimeout with "10" text
  And Fill tbSubPartnerId with "868" text
  And Fill tbCommissionRate with "5" text
  
  And Click btnHash

  And Click btnPost

  And Wait for 10 seconds

  And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
  And Execute mmSubPartnerStatus sql and check "1" text and "CustomFlowState" column
  And Execute mmSubPartnerStatus sql and check "10" text and "TwoStepTimeout" column
  And Execute mmSubPartnerPayment_Sql sql and check "56" text and "State" column

  And Fill "password" with "a" text
  And Click "submit"

  And Wait for 10 seconds

  And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
  And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowState" column
  And Execute mmSubPartnerPayment_Sql sql and check "58" text and "State" column

  And Execute "MMSP_xmlData" sql and write "Data" column to "MMSP_xmlFilePath" file


  #  Then Close browser



@mmsubppartnerpayment @happypath
Scenario: 2.SubPartnerPayment Approve
  When Open the Firefox and go to sgatetest

  And Click ddlService

  And Click "mmtpsApprove" element
  And Click btnSetInputType

  And Clear "inputField" element

  And Get value and set to mmSubPartner3DSApprovment with "MMSP_xmlFilePath" file

  And Click btnSend

  And Wait for 5 seconds

  And "outputField" element contains "Approved:" string

  And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
  And Execute mmSubPartnerStatus sql and check "5" text and "CustomFlowState" column
  And Execute mmSubPartnerPayment_Sql sql and check "100" text and "State" column

  #  Then Close browser



@mmsubppartnerpayment @happypath
Scenario: 3.SubPartnerPayment Release
  When Open the Firefox and go to sgatetest

  And Click ddlService

  And Click "mmtpsApprove" element
  And Click btnSetInputType

  And Clear "inputField" element

  And Execute ccProxySale3DsecObjectId sql and set ReleasePayment xml

  And Click btnSend

  And "outputField" element contains "Success" string

  And Execute releaseControl sql and check statusId

  #  Then Close browser




  @mmsubppartnerpaymentfail
  Scenario: 1.SubPartnerPayment

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCMarketPlace" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "5818775818772285" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "Otomasyon" text
    And Fill tbPaymentContent with "MMSUBPARTNERPAYMENT" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text
    And Fill tbSubPartnerId with "868" text
    And Fill tbCommissionRate with "5" text

    And Fill tbCustomerName with "HepsiGecerli" text
    And Fill tbCustomerSurname with "Erdogan" text
    And Fill tbCustomerEmail with "goktu.erdogan@wirecard.com" text
    And Fill tbLanguage with "TR" text

    And Click btnHash

    And Click btnPost

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmSubPartnerStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmSubPartnerPayment_Sql sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "58" text and "State" column

    And Execute "MMSP_xmlData" sql and write "Data" column to "MMSP_xmlFilePath" file



  @mmsubppartnerpaymentfail
  Scenario: 2.SubPartnerPayment Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to mmSubPartner3DSApprovment with "MMSP_xmlFilePath" file

    And Click btnSend

    And Wait for 5 seconds

    And "outputField" element contains "Approved:" string

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "5" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "100" text and "State" column

  #  Then Close browser



  @mmsubppartnerpaymentfail
  Scenario: 3.SubPartnerPayment Release
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Execute ccProxySale3DsecObjectId sql and set ReleasePayment xml

    And Click btnSend

    And "outputField" element contains "Success" string

    And Execute releaseControl sql and check statusId


  @mmsubppartnerpaymentfail
  Scenario: 1.SubPartnerPayment

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCMarketPlace" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "5818775818772285" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "Otomasyon" text
    And Fill tbPaymentContent with "MMSUBPARTNERPAYMENT" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text
    And Fill tbSubPartnerId with "868" text
    And Fill tbCommissionRate with "5" text

    And Fill tbCustomerName with "MailHatali" text
    And Fill tbCustomerSurname with "Erdogan" text
    And Fill tbCustomerEmail with "asdasd" text
    And Fill tbLanguage with "EN" text

    And Click btnHash

    And Click btnPost

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmSubPartnerStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmSubPartnerPayment_Sql sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "58" text and "State" column

    And Execute "MMSP_xmlData" sql and write "Data" column to "MMSP_xmlFilePath" file


  @mmsubppartnerpaymentfail
  Scenario: 1.SubPartnerPayment

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCMarketPlace" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "5818775818772285" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "Otomasyon" text
    And Fill tbPaymentContent with "MMSUBPARTNERPAYMENT" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text
    And Fill tbSubPartnerId with "868" text
    And Fill tbCommissionRate with "5" text

    And Fill tbCustomerName with "DilHatali" text
    And Fill tbCustomerSurname with "Erdogan" text
    And Fill tbCustomerEmail with "goktu.erdogan@wirecard.com" text
    And Fill tbLanguage with "asd" text

    And Click btnHash

    And Click btnPost

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmSubPartnerStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmSubPartnerPayment_Sql sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "58" text and "State" column

    And Execute "MMSP_xmlData" sql and write "Data" column to "MMSP_xmlFilePath" file

  @mmsubppartnerpaymentfail
  Scenario: 2.SubPartnerPayment Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to mmSubPartner3DSApprovment with "MMSP_xmlFilePath" file

    And Click btnSend

    And Wait for 5 seconds

    And "outputField" element contains "Approved:" string

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "5" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "100" text and "State" column

  #  Then Close browser



  @mmsubppartnerpaymentfail
  Scenario: 3.SubPartnerPayment Release
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Execute ccProxySale3DsecObjectId sql and set ReleasePayment xml

    And Click btnSend

    And "outputField" element contains "Success" string

    And Execute releaseControl sql and check statusId


  @mmsubppartnerpaymentfail
  Scenario: 1.SubPartnerPayment

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCMarketPlace" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "5818775818772285" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "Otomasyon" text
    And Fill tbPaymentContent with "MMSUBPARTNERPAYMENT" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text
    And Fill tbSubPartnerId with "868" text
    And Fill tbCommissionRate with "5" text

    And Fill tbCustomerName with "AdDoluSoyadBos" text
    And Fill tbCustomerSurname with "" text
    And Fill tbCustomerEmail with "goktu.erdogan@wirecard.com" text
    And Fill tbLanguage with "TR" text

    And Click btnHash

    And Click btnPost

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmSubPartnerStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmSubPartnerPayment_Sql sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "58" text and "State" column

    And Execute "MMSP_xmlData" sql and write "Data" column to "MMSP_xmlFilePath" file


  @mmsubppartnerpaymentfail
  Scenario: 2.SubPartnerPayment Approve
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Get value and set to mmSubPartner3DSApprovment with "MMSP_xmlFilePath" file

    And Click btnSend

    And Wait for 5 seconds

    And "outputField" element contains "Approved:" string

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "5" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "100" text and "State" column

  #  Then Close browser


  @mmsubppartnerpaymentfail
  Scenario: 3.SubPartnerPayment Release
    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "mmtpsApprove" element
    And Click btnSetInputType

    And Clear "inputField" element

    And Execute ccProxySale3DsecObjectId sql and set ReleasePayment xml

    And Click btnSend

    And "outputField" element contains "Success" string

    And Execute releaseControl sql and check statusId


  @mmsubppartnerpaymentfail
  Scenario: 1.SubPartnerPayment

    When Open the Firefox and go to mmtspUrl

    And Fill tbServiceType with "CCMarketPlace" text
    And Fill tbOpertaionType with "Sale3DSEC" text
    And Fill tbUserCode with "20104" text
    And Fill tbPin with "189A3B7DF0DC49718B2612zly" text
    And Fill tbPrice with "100" text
    And Fill tbOwnerName with "otomasyon" text
    And Fill tbCardNo with "5818775818772285" text
    And Fill tbExpireYear with "20" text
    And Fill tbExpireMonth with "12" text
    And Fill tbCvv with "001" text
    And Fill tbInstallmentCount with "0" text
    And Fill tbMPAY with "Otomasyon" text
    And Fill tbPaymentContent with "MMSUBPARTNERPAYMENT" text
    And Fill tbErrorUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbSuccessUrl with "https://test.wirecard.com.tr/pgate/testgetpage.aspx" text
    And Fill tbCurrency with "TRY" text
    And Fill tbPosLang with "tr" text
    And Fill tbHashRandomParam with "123456" text

    And Fill tbCustFlowType with "2" text
    And Fill tbTwoStepTimeout with "10" text
    And Fill tbSubPartnerId with "868" text
    And Fill tbCommissionRate with "5" text

    And Fill tbCustomerName with "HepsiBosDilTR" text
    And Fill tbCustomerSurname with "" text
    And Fill tbCustomerEmail with "" text
    And Fill tbLanguage with "TR" text

    And Click btnHash

    And Click btnPost

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "1" text and "CustomFlowState" column
    And Execute mmSubPartnerStatus sql and check "10" text and "TwoStepTimeout" column
    And Execute mmSubPartnerPayment_Sql sql and check "56" text and "State" column

    And Fill "password" with "a" text
    And Click "submit"

    And Wait for 10 seconds

    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowType" column
    And Execute mmSubPartnerStatus sql and check "2" text and "CustomFlowState" column
    And Execute mmSubPartnerPayment_Sql sql and check "58" text and "State" column

    And Execute "MMSP_xmlData" sql and write "Data" column to "MMSP_xmlFilePath" file


