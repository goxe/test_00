@test @container2
Feature: CCMarketPlaceSale3DSEC - Default

# Happy Path - Owner Name Bos - MPAY Bos - Payment Content Bos - Sub Partner Id AN - Sub Partner Id Bos - Commission Rate AN - Commission Rate Bos - Kart No AN - Kart No Bos
# - Installment Count 13 - Installment Count AN - Installment Count 3 (Basarili) - Installment Count 0 (Basarili) - Installment Count Bos (Basarili) - Price AN - Price Bos
# - CVV Bos (Basarili Islem) - Expire Month Bos - Expire Year Bos

  @marketplace3dsec @happypath
  Scenario: CCMarketPlaceSale3DSEC

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
    And "outputField" element contains "RedirectURL" string
    And Execute ccMarketPlaceSale3DSEC_Sql sql and check "42" text and "State" column

    #  Then Close browser


  @marketplace3dsec @happypath
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccMarketPlaceSale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string
    And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Owner Name Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_OwnerName_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Expire Year Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_ExpireYear_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00002" string
    And "outputField" element contains "son kullan" string


    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Expire Month Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_ExpireMonth_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00002" string
    And "outputField" element contains "son kullan" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - CVV Bos (Basarili Islem)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_Cvv_Bos" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
#  And "outputField" element contains "MaskedCreditCardNumber" string
#  And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccMarketPlaceSale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string
    And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Price Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_Price_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Price AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_Price_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Installment Count Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_InstallmentCount_Bos" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
#  And "outputField" element contains "MaskedCreditCardNumber" string
#  And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccMarketPlaceSale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string
    And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Installment Count 0 (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_InstallmentCount_0" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
#  And "outputField" element contains "MaskedCreditCardNumber" string
#  And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccMarketPlaceSale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string
    And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Installment Count 3 (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_InstallmentCount_3" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
#  And "outputField" element contains "MaskedCreditCardNumber" string
#  And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: Yonlendirmeli Link ve 3DSecure

    Given Execute ccMarketPlaceSale3DSEC_Sql sql and go "ccProxySale3DSecUrl" url with "ObjectId"

    And Fill "3dSecure" element with "a" text
    And Click "3dSecureGonder" element

    And "3DSecResultMessage" element contains "Approved:" string
    And Execute ccMarketPlaceSale3DSEC_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Installment Count AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_InstallmentCount_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Installment Count 13

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_InstallmentCount_13" element
    And Click btnSend

    And "outputField" element contains "taksit se" string

#  And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Kart No Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_CardNo_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

#  And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Kart No AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_CardNo_AN" element
    And Click btnSend

    And "outputField" element contains "CCVC00001" string
    And "outputField" element contains "bir kart numaras" string

#  And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Commission Rate Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_CommissionRate_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00004" string
    And "outputField" element contains "null veya bo" string

#  And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Commission Rate AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_CommissionRate_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Sub Partner Id Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_SubPartnerId_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Sub Partner Id AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_SubPartnerId_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec
  Scenario: CCmarketplace3dsec - Payment Content Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_PaymentContent_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplace3dsec @ignore
  Scenario: CCmarketplace3dsec - MPAY Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceSale3DSecXml_MPAY_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser