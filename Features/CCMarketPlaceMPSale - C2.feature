@test @container2
Feature: CCMarketPlaceMPSale - Default



  @marketplacempsale @happypath
  Scenario: CCMarketPlaceMPSale

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
    And "outputField" element contains "MaskedCreditCardNumber" string
    And Execute ccMarketPlaceSale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Owner Name Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_OwnerName_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser

  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Expire Year Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_ExpireYear_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Expire Month Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_ExpireMonth_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - CVV Bos (Basarili Islem)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_Cvv_Bos" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
    And "outputField" element contains "MaskedCreditCardNumber" string
    And Execute ccMarketPlaceSale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Price Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_Price_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Price AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_Price_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Installment Count Bos (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_InstallmentCount_Bos" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
    And "outputField" element contains "MaskedCreditCardNumber" string
    And Execute ccMarketPlaceSale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Installment Count 0 (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_InstallmentCount_0" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
    And "outputField" element contains "MaskedCreditCardNumber" string
    And Execute ccMarketPlaceSale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Installment Count 3 (Basarili)

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_InstallmentCount_3" element
    And Click btnSend

    And "outputField" element contains "OrderId" string
    And "outputField" element contains "MaskedCreditCardNumber" string
    And Execute ccMarketPlaceSale_Sql sql and check "100" text and "State" column

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Installment Count AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_InstallmentCount_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Installment Count 13

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_InstallmentCount_13" element
    And Click btnSend

    And "outputField" element contains "Hata!" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Kart No Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_CardNo_Bos" element
    And Click btnSend

    And "outputField" element contains "CCPCT0001" string
    And "outputField" element contains "Kredi kart" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Kart No AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_CardNo_AN" element
    And Click btnSend

    And "outputField" element contains "CCVC00001" string
    And "outputField" element contains "bir kart numaras" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Commission Rate Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_CommissionRate_Bos" element
    And Click btnSend

    And "outputField" element contains "CCVC00004" string
    And "outputField" element contains "null veya bo" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Commission Rate AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_CommissionRate_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Sub Partner Id Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_SubPartnerId_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Sub Partner Id AN

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_SubPartnerId_AN" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - Payment Content Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_PaymentContent_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser


  @marketplacempsale
  Scenario: CCMarketPlaceMPSale - MPAY Bos

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCProxySale" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "ccMarketPlaceMPSale_MPAY_Bos" element
    And Click btnSend

    And "outputField" element contains "WDSG00005" string

    #  Then Close browser