@test @ignore
Feature: AddSubPartner Yuzyuze - Sgate

  @addsubpartnerwsyy @de @ignor
  Scenario: SubPartner Risk Mail Gonderimi Kapat

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Go programAyarlariAdvancedKullanicilarGoktugDuzenleUrl url
#    And Click "programAyarlariAdvanced" element
#    And Click "programAyarlariAdvancedKullanicilar" element
#    And Click "asdasd" css
#    And Wait for 3 seconds
#    And Click "programAyarlariAdvancedKullanicilarGoktug" element
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And Force uncheck ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName
    And Click ctl00_MainContent_btnSubmit
    And Click "programAyarlariAdvancedKullanicilarGoktugDuzenle" element
    And ctl00_MainContent_CtrlRoles_ctl04_CtrlRoleName checkbox is not selected

    Then Close browser



  @addsubpartnerwsyy @de @ignor
  Scenario: Switch Uzaktan - Yuzyuze

    When Open the Firefox and go to testAdminPanel
    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Click "uyeSirketler" element
    And Fill "uyeSirketlerSirketId" element with "12356" text
    And Click ctl00_cphSolMenu_btnAra
    And Click "uyeSirketlerIsyeriBilgileriniDuzenle" element
    And Select ctl00_MainContent_rbtftf radio button
    And Find "dogrulamaKaydet" element and send return
    And ctl00_MainContent_rbtftf checkbox is selected

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Individual Zorunlu

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivZorunluDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser
    


  @addsubpartnerwsyy @de
  Scenario: Pending Onay

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Execute subPartnerControl sql and go "subPartnerOnayYuzyuze" url with "Id" in current page

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Individual Zorunlu Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivZorunluDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Individual Tam Data

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivTamDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser




  @addsubpartnerwsyy @de
  Scenario: Pending Onay

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Execute subPartnerControl sql and go "subPartnerOnayYuzyuze" url with "Id" in current page

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Individual Tam Data Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerIndivTamDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Private Zorunlu

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateZorunluDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: Pending Onay

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Execute subPartnerControl sql and go "subPartnerOnayYuzyuze" url with "Id" in current page

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Private Zorunlu Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateZorunluDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Private Tam Data

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateTamDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser



  @addsubpartnerwsyy @de
  Scenario: Pending Onay

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Execute subPartnerControl sql and go "subPartnerOnayYuzyuze" url with "Id" in current page

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Private Tam Data Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerPrivateTamDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Corporation Zorunlu

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpZorunluDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser



  @addsubpartnerwsyy @de
  Scenario: Pending Onay

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Execute subPartnerControl sql and go "subPartnerOnayYuzyuze" url with "Id" in current page

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Corporation Zorunlu Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpZorunluDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Corporation Tam Data

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpTamDataXml" and replace subPartnerUniqueID with date
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser



  @addsubpartnerwsyy @de
  Scenario: Pending Onay

    When Open the Firefox and go to subPartnerAddUI

    And Fill cbpLogin_txtKullaniciAdi with "goktug.erdogan" text
    And Fill cbpLogin_txtSifre with "Test123*" text
    And Click cbpLogin_btnLogin

    And Execute subPartnerControl sql and go "subPartnerOnayYuzyuze" url with "Id" in current page

    And Click ctl00_MainContent_btnConfirm
    And Click "subPartnerPendingOnaylandi" element

    Then Close browser


  @addsubpartnerwsyy @de
  Scenario: AddSubPartner - Corporation Tam Data Update

    When Open the Firefox and go to sgatetest

    And Click ddlService

    And Click "CCEmailServiceAddSaleRequest" element
    And Click btnSetInputType

    And Clear "inputField" element
    And Fill "inputField" element with "addSubPartnerCorpTamDataUpdateXml" and execute subPartnerControl sql
      | SPID               | Id      |
      | subPartnerUniqueID | time    |
    And Click btnSend

    And "outputField" element contains "Success" string

    Then Close browser
    
